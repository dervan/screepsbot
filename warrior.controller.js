var utils = require('utils');
var manager = require('rooms.manager');

var print = utils.print;
var ERR = utils.ERR;
var genParts = utils.genParts;

var smallRangedFighter = {
    parts:          genParts(3, TOUGH).concat(
                    genParts(6, MOVE)).concat(
                    genParts(3, RANGED_ATTACK)),
    name: "WSRF",
    memory: {
        role: "warrior",
        type: "ranged"
    }
};

var mediumFighter = {
    parts:          genParts(7, MOVE).concat(
                    genParts(7, ATTACK)),
    name: "WBF",
    memory: {
        role: "warrior",
        type: "melee"
    }
};

var warriorController = {
    name: "Warrior Controller",

    roleName: "warrior",

    initMemory: function() {
        Memory.warrior = {};
        Memory.warrior.queue = [];
    },

    prepare: function() {
        if(Memory.warrior == null || Memory.warrior.queue == null){
            print(ERR, "Warrior not initialized!");
            this.priority = 0;
        }else if(this.creepsCount() < 1){
            //|| Memory.warrior.queue.length > 0) //always keep one warrior alive, just in case
            //this.priority = 0.99; // MUST be smaller than harvest for emergencies!
            this.priority = 1.00;
        }else{
            this.priority = 0.05;
        }// Before we come up with a wiser way of deciding how many warriors we want, use an idle priority lower than upgrader to aviod starving upgraders      e
        var rooms = manager.getControlledRooms();
        if(utils.getMainLevel() < 3){
            this.priority = 0;
        }
        
        if(!Memory.warrior)
            Memory.warror = {};
        if(!Memory.warrior.queue)
            Memory.warrior.queue = [];
            
        if(Memory.warrior.queue.length == 0)
        {
            var enemies = manager.findInAllRooms(FIND_HOSTILE_CREEPS);
            for(e in enemies)
            {
                Memory.warrior.queue.push(this.getPartsBasedOnEnemy(e));
            }
        }
        
        this.creep = smallRangedFighter;
        if(Memory.warrior.queue.length > 0)
        {
            this.creep = _.head(Memory.warrior.queue);
        }

        this.creep = mediumFighter; // ???
        this.creepCost = utils.calculateSpawnCost(this.creep.parts);
    },

    getPriorities: function(){
        var energies = manager.getRoomsMaxEnergy();
        var prio = _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
        return _.zipWith(energies, prio, (e,p)=>(e<1000)?0.0:p);
    },

    getPriority: function(roomName, energyCapacity) {
        if(this.priority == null){
            print(ERR, "Priority not calculated in " + this.name);
            return -0.01;
        }else{
            return this.priority;
        }
    },

    underAttack: function() {
        var hostiles = manager.findInAllRooms(FIND_HOSTILE_CREEPS);
        return hostiles.length > 0;
    },

    isHealer: function(creep) {
        //TODO
    },

    getPartsBasedOnEnemy: function(enemy) {
        //Try to guess what type of unit is enemy (ranged, melee or healer)
        //and create stronger/equal counterpart
        var dict = {};

        dict[TOUGH] = 0;
        dict[CARRY] = 0;
        dict[WORK] = 0;
        dict[ATTACK] = 0;
        dict[HEAL] = 0;
        dict[RANGED_ATTACK] = 0;

        for(part in enemy.parts)
        {
            if(dict[part])
                dict[part]++;
            else
                dict[part] = 1;
        }

        var res = [];
        var type;

        //WORK is used to dismantle structures!!!
        if(dict[ATTACK]+dict[WORK] >= dict[RANGED_ATTACK] && dict[ATTACK]+dict[WORK] > dict[HEAL])
        {
            var cnt = dict[ATTACK]+dict[WORK]+dict[RANGED_ATTACK]+1;
            cnt = Math.min(cnt, 25);
            res.concat(genParts(cnt, ATTACK));
            type = "melee";
        }
        else
        if(dict[RANGED_ATTACK] >= dict[ATTACK] && dict[RANGED_ATTACK] >= dict[HEAL])
        {
            var cnt = dict[RANGED_ATTACK]+1;
            cnt = Math.min(cnt, 25);
            res.concat(genParts(cnt, RANGED_ATTACK));
            type = "ranged";
        }
        else
        if(dict[HEAL] >= dict[RANGED_ATTACK] && dict[HEAL] >= dict[ATTACK])
        {
            var cnt = dict[HEAL]+1;
            cnt = Math.min(cnt, 25);
            res.concat(genParts(cnt, HEAL));
            type = "heal";
        }

        res.concat(genParts(dict[TOUGH], TOUGH));
        res.concat(genParts(res.length, MOVE));

        var cmp = {};

        cmp[TOUGH] = 0;
        cmp[CARRY] = 1;
        cmp[WORK] = 2;
        cmp[ATTACK] = 3;
        cmp[HEAL] = 4;
        cmp[RANGED_ATTACK] = 5;

        res.sort (
             function(a, b) {
                cmp[a] < cmp[b];
             });

        var creep = {
            parts: res,
            name: "WG", //warrior generated
            memory: {
                role: this.roleName,
                type: type
            }
        };

        return res;
    },

    getEnergyThreshold: function(){
        return 780;
    },

    getIdealCreep: function(energy, roomName){
        if(utils.getMainLevel() >= 3 && this.creepCost <= energy)
            return mediumFighter;
        else
            return null;
    },

    locateHostiles: function(room) {
        var hostiles = room.find(FIND_HOSTILE_CREEPS);
        return hostiles;
    },

    run: function(room) {
        if(Memory.warrior == null){
            Memory.warrior = {};
        }
        if(Memory.warrior.queue == null){
            Memory.warrior.queue = [];
        }
        var creeps = this.getCreeps();
        for(var i=0; i < creeps.length; i++)
        {
            this.move(creeps[i]);
        }
        var hostiles = manager.findInAllRooms(FIND_HOSTILE_CREEPS);
        for(var h in hostiles){
            var hostile = hostiles[h];
            var towers = hostile.room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            towers.forEach((tower) => {
                if(tower.pos.getRangeTo(hostile)<100){
                    tower.attack(hostile);
                }
            });
            Memory.attackedRoom = hostile.room.name;
        }
        var towers = manager.findStructures(STRUCTURE_TOWER, manager.getControlledRooms());
        for(var t in towers){
            var t = towers[t];
            var creepsToHeal = t.room.find(FIND_MY_CREEPS, {filter:(c)=>c.hits<c.hitsMax});
            if(creepsToHeal != null){
                t.heal(creepsToHeal[0]);
            }
        }
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    moveMelee: function(creep) {
        var target = this.locateHostiles(creep.room)[0];
        if(target == null){
            return utils.creepMoveToRandom(creep);
        }else{
            var res = creep.attack(target);
            if(res == ERR_NOT_IN_RANGE)
                creep.moveTo(target);
        }
    },

    moveRanged: function(creep) {
        var target = this.locateHostiles(creep.room)[0];
        if(target == null){
            return utils.creepMoveToRandom(creep);
        }else{
            var res = creep.rangedAttack(target);
            if(res == ERR_NOT_IN_RANGE)
                creep.moveTo(target);
            if(res == OK && creep.pos.getRangeTo(target) <= 2)
            {
                var xDir = creep.pos.x-target.pos.x;
                var yDir = creep.pos.y-target.pos.y;
                if(xDir != 0) xDir = xDir/Math.abs(xDir);
                if(yDir != 0) yDir = yDir/Math.abs(yDir);
                var dir = utils.diffToDirection(xDir, yDir);
                creep.move(dir);
                creep.say("Kiting!");
            }

        }

    },

    moveHeal: function(creep) {
        //TODO
    },

    move: function(creep) {
        if(Memory.attackedRoom != undefined &&
           creep.room.name != Memory.attackedRoom){
            var room = Game.rooms[Memory.attackedRoom];
            if(room != null){
                var target = this.locateHostiles(room)[0];
            }
            if(room == null || target == null){
                var target = new RoomPosition(25, 25, Memory.attackedRoom);
            }
            if(target == null) return;
            creep.moveTo(target);
            return;
        }else{
            if(creep.memory.type == "melee"){
                this.moveMelee(creep);
                return;
            }else if(creep.memory.type == "ranged"){
                this.moveRanged(creep);
            }
        }
        return utils.creepMoveToRandom(creep);
    },
};

module.exports = warriorController;
