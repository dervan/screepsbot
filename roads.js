var manager = require('rooms.manager');
var unwalkables = function(s){
    //return s.structureType == STRUCTURE_WALL || s.structureType == STRUCTURE_EXTENSION;
    return OBSTACLE_OBJECT_TYPES.includes(s.structureType);
}

var roads = {
    
    init: function(){
        if(Memory.road_requests == null)
            Memory.road_requests = [];
        if(Memory.road_requests_pending == null)
            Memory.road_requests_pending = {};
    },
    
    run: function(){
        this.init();
        this.autorequest();
        this.build();
    },
    
    // Priority is a value from 0 to 10, with 10 being most likely to be build first, name is an optional comment.
    // Road will be build starting at pos1 TOWARDS pos2, so you may want to optimize the order.
    request: function(pos1, pos2, priority, name){
        // TODO: Priority inheritance.
        if(Memory.road_requests_pending[pos1.serialize() + "$" + pos2.serialize()] ||
           Memory.road_requests_pending[pos2.serialize() + "$" + pos1.serialize()]){
               return;
           }
        console.log("Requesting a new road from " + pos1.serialize() + " to " + pos2.serialize() + " with priority " + priority + " \"" + name + "\"");
        Memory.road_requests.push({
            pos1: pos1,
            pos2: pos2,
            priority: priority,
            name: name,
            state: 'new'
        });
        Memory.road_requests_pending[pos1.serialize() + "$" + pos2.serialize()] = true;
    },
    
    build: function(){
        if(Memory.road_current == null){
            //console.log("No road build pending. Picking a new road request to build");
            if(Memory.road_requests.length == 0)
                return; // Nothing to build.
            var newr = _.filter(Memory.road_requests, (r) => r.state == 'new');
            var r = _.max(newr, (r) => r.priority);
            var max_prio = r.priority;
            var r = _.sample(_.filter(newr, (r) => r.priority == max_prio));
            if(r == null) return;
            Memory.road_current = JSON.parse(JSON.stringify(r));
        }
        if(Memory.road_current.path == null){
            console.log("Currently built road has no path.");
            var p1 = RoomPosition.fromObj(Memory.road_current.pos1);
            var p2 = RoomPosition.fromObj(Memory.road_current.pos2);
            //console.log("Looking for a path between " + p1.serialize() + " and " + p2.serialize());
            var path = PathFinder.search(p1, p2, {roomCallback: function(roomName, matrix){
                let room = Game.rooms[roomName];
                if(!room) return;
                matrix = new PathFinder.CostMatrix;
                // TODO: Cache these CostMatrices!
                room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_ROAD}).forEach((s) => {
                    matrix.set(s.pos.x, s.pos.y, 0x01);
                });
                room.find(FIND_STRUCTURES, {filter: unwalkables}).forEach((s) => {
                    matrix.set(s.pos.x, s.pos.y, 0xff);
                });
                return matrix;
            }, plainCost: 2, swampCost: 3, ignoreCreeps: true, maxOps: 3000}).path;
            // Test this path, because the pathfidner is super silly.
            //let last = path.length - 1;
            //let v = path[last];
            //console.log(v.serialize());
            
            Memory.road_current.path = path;
        }
        var path = _.map(Memory.road_current.path, (rp) => RoomPosition.fromObj(rp));
        this.drawPath(path);
        
        // The following is CPU consuming and does not need to be called too frequently.
        if(Game.time % 4 != 0) return;
        // Now, walk through the path.
        // If you see an empty space, place construction site.
        // Stop when 10 csites are ready.
        // If entire path is ready, finish building and proceed to another route.
        // If there is a permanent obstruction, abort the path.
        let path_len = path.length;
        let roads = 0;
        let csites_limit = 10;
        let abort_path = false;
        for(var i in path){
            let pos = path[i];
            if(Game.rooms[pos.roomName] == null){
                // Room inaccessible. Try again later.
                return;
            }
            // Is it impossible to enter this position?
            if(!pos.isEnterable()){
                abort_path = true;
                break;
            }
            // Is this an edge? Count it as road.
            if(pos.isOnEdge()){
                path_len--;
                continue;
            }
            // Is there a road already?
            if(_.filter(pos.lookFor(LOOK_STRUCTURES), (s) => s.structureType == STRUCTURE_ROAD).length > 0){
                roads++;
                continue;
            }
            // Is there a road construction site?
            let csites = _.filter(pos.lookFor(LOOK_CONSTRUCTION_SITES), (s) => s.structureType == STRUCTURE_ROAD);
            if(csites.length > 0){
                csites_limit -= 1;
                continue;
            }else{
                if(csites_limit > 0){
                    // We can place a construction site here.
                    console.log("Placing road construction site at " + pos);
                    var res = pos.createConstructionSite(STRUCTURE_ROAD);
                    csites_limit -= 1;
                    if(res == ERR_FULL){
                        // Whatever.
                        csites_limit = 0;
                    }
                }
            }
        };
        
        if(roads >= path_len){
            console.log("Road is complete!");
            // Mark the road request as complete!
            var q = _.find(Memory.road_requests, (r) => {
                let a1 = r.pos1; let b1 = Memory.road_current.pos1;
                let a2 = r.pos2; let b2 = Memory.road_current.pos2;
                let p = (a1.x == b1.x && a1.y == b1.y && a1.roomName == b1.roomName);
                let q = (a2.x == b2.x && a2.y == b2.y && a2.roomName == b2.roomName);
                return p && q;
            } );
            if(q == null){
                console.log("WARNING: Current road request not found on the list!");
                return;
            }else{
                q.state = 'done';
            }
            abort_path = true;
        }
        
        if(abort_path){
            // Remove the current request.
            delete Memory.road_current;
        }
        console.log("Current road build progress: " + roads + "/" + path_len);
        
    },
    
    drawPath: function(path){
        if(path.length == 0) return;
        // Split this path into chunks per room.
        var chunks = [];
        var current_chunk = [];
        var current_room = path[0].roomName;
        for(var i in path){
            if(path[i].roomName == current_room){
                current_chunk.push(path[i]);
            }else{
                chunks.push(current_chunk);
                current_chunk = [];
                current_room = path[i].roomName;
                current_chunk.push(path[i]);
            }
        }
        chunks.push(current_chunk);
        // Now draw each chunk spearately.
        for(var i in chunks){
            var q = chunks[i][0];
            if(q == null) continue;
            var room = Game.rooms[q.roomName];
            if(room == null) continue;
            room.visual.poly(chunks[i], {lineStyle: 'dotted'});
        }
    },
    
    // These functions scan Memory and heuristically decide which room-positions are worth requesting roads. 
    autorequest: function(){
        if(Game.time % 30 == 3)
            this.autorequest_harvest();
        if(Game.time % 30 == 13)
            this.autorequest_controllers();
    },
    
    autorequest_harvest: function(){
        for(var s in Memory.harvest.sources){
            s = Memory.harvest.sources[s];
            var sourcepos = RoomPosition.fromObj(s.room_pos);
            
            // Look for spawn in same room.
            var room = Game.rooms[sourcepos.roomName];
            if(room != null){
                var spawns = room.find(FIND_MY_STRUCTURES, {filter: { structureType: STRUCTURE_SPAWN}});
                if(spawns.length > 0){
                    // TODO: Repeat per each spawn.
                    var spawn = spawns[0];
                    this.request(spawn.pos, sourcepos, 8.5, "Spawn -> Source in same room");
                    continue; // Do not search for nearby rooms.
                }
            }
            
            var roomName = manager.getClosestRoom(sourcepos.roomName, manager.OWN);
            var room = Game.rooms[roomName];
            if(room == null)
                continue;
            var spawns = room.find(FIND_MY_STRUCTURES, {filter: { structureType: STRUCTURE_SPAWN}});
            if(spawns.length == 0)
                continue; // no spawn in this room
            var spawn = spawns[0];
            this.request(spawn.pos, sourcepos, 9, "Spawn -> Source in next room");
        }
    },
    
    autorequest_controllers: function(){
        var ownedRooms = manager.getControlledRooms();
        for(var room in ownedRooms){
            room = Game.rooms[ownedRooms[room]];
            if(!room) continue;
            var controller = room.controller;
            room.find(FIND_MY_STRUCTURES, {filter: { structureType: STRUCTURE_SPAWN}}).forEach((s) => {
                this.request(s.pos, controller.pos, 5, "Spawn -> Controller");
            });
        }
    }
};

module.exports = roads;
