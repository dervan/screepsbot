var utils = require('utils');

// Must be greter than creep TTL or harvest sources would not be considered POIs.
var POI_REMOVE_TIME = 1900;

// The POI route matrix size will be the following number squared!
var POI_TOP_N = 40;

var poi = {
    preparePrototypes: function(){
        var POIaccessor = this.accessPOI;
        Creep.prototype.moveToPOI = function(target, opts){
            var dest = target;
            if(dest.pos != null) dest = dest.pos;
            if(this.memory._move != null){
                // Creep switched destination.
                var prev = this.memory._move.dest;
                if(prev.x != dest.x || prev.y != dest.y || prev.roomName != dest.room)
                    POIaccessor(dest);
            }else{
                // First move this creep does.
                POIaccessor(dest);
            }
            // Actually move the creep.
            if(opts == null) opts = {};
            return this.moveTo(target, opts);
        };
        
        RoomPosition.prototype.serialize = function(){
            return this.roomName + "|" + this.x + "|" + this.y;
        };
        RoomPosition.deserialize = function(string){
            var a = string.split("|");
            return new RoomPosition(a[1],a[2],a[0]);
        };
        
        delete Memory.bestPoi;
        delete Memory.poiset;
        delete Memory.pois;
        delete Memory.poiAll;
        if(Memory.poiDetails == null)
            Memory.poiDetails = {};
        if(Memory.poiBest == null)
            Memory.poiBest = new Array(POI_TOP_N);
        if(Memory.poiBestSet == null)
            Memory.poiBestSet = {};
        if(Memory.poiBestCnt == null)
            Memory.poiBestCnt = 0;
    },
    
    accessPOI: function(position){
        var poskey = position.serialize();
        if(Memory.poiDetails[poskey]){
            Memory.poiDetails[poskey].la = Game.time;
            Memory.poiDetails[poskey].n = Memory.poiDetails[poskey].n + 1;
        }else{
            console.log("New POI " + poskey);
            Memory.poiDetails[poskey] = {
                la: Game.time,
                fa: Game.time,
                n: 1
            };
        }
    },
    
    cleanupPOIs: function(){
        console.log("Cleaning up pois");
        var newpoi = {};
        for(var poiname in Memory.poiDetails){
            var details = Memory.poiDetails[poiname];
            if(details.la < Game.time - POI_REMOVE_TIME) continue;
            newpoi[poiname] = details;
        }
        Memory.poiDetails = newpoi;
        
        for(var i in Memory.poiBest){
            var str = Memory.poiBest[i];
            if(!newpoi[str]) Memory.poiBest[i] = null;
        }
    },
    
    clearPoiBest: function(){
        Memory.poiBest = new Array(POI_TOP_N);
        Memory.poiBestSet = {};
        Memory.poiBestCnt = 0;
    },
    
    evalfunc: function(details){
        var time = Game.time - details.fa;
        if(time < 500) return 0;
        return details.n * 1.0 / time;
    },
    
    removePoiBest: function(n){
        if(n < 0) return;
        var p = Memory.poiBest[n];
        Memory.poiBest[n] = null;
        delete Memory.poiBestSet[p];
        Memory.poiBestCnt--;
    },
    insertPoiBest: function(poi){
        // Find a free N.
        var n = -1;
        for(var i = 0; i < POI_TOP_N; i++){
            if(Memory.poiBest[i] == null){
                n = i; break;
            }
        }
        console.log("New poiBest: " + poi + " at N = " + n);
        Memory.poiBest[n] = poi;
        Memory.poiBestSet[poi] = true;
        Memory.poiBestCnt++;
    },
    
    updatePoiBest: function(){
        //this.clearPoiBest();
        
        // TODO: Instead of evalfunc, use a more local value, e.g (time since last accessed)^-1, smoothed out with a very soft low-pass filter.
        // This way we'll focus on recent frequency, instead of sticking to very old statistics.
        
        // Find worst out of best
        var worst = _.min(Memory.poiBest, (item) => {
            if(item == null) return Infinity;
            var details = Memory.poiDetails[item];
            if(details == null) return Infinity;
            return this.evalfunc(details);
        })
        if(worst != null){
            // Find best out of ordinary
            var candidate = _.max(Memory.poiDetails, (details, key) => {
                if(Memory.poiBestSet[key]) return 0;
                return this.evalfunc(details);
            });
            var worstDetails = Memory.poiDetails[worst];
            // If new is better, remove the worst!
            if(candidate != null && this.evalfunc(worstDetails) < this.evalfunc(candidate)){
                console.log(this.evalfunc(worstDetails));
                console.log(this.evalfunc(candidate));
                var i = _.indexOf(Memory.poiBest, worst);
                console.log("Removing the worst poi " + worst + " from best pois.");
                this.removePoiBest(i);
                // DO NOT REPEAT this process! That would remove too much when a new attractive POI appears.
            }
        }
        
        // Now introduce a new poiBest, as long as there is free space.
        while(Memory.poiBestCnt < POI_TOP_N && Memory.poiBest.length < Object.keys(Memory.poiDetails).length){
            // Find a good candidate among ordinary ones
            var candidate = _.max(Memory.poiDetails, (details, key) => {
                if(Memory.poiBestSet[key]) return 0;
                return this.evalfunc(details);
            });
            var pos = _.find(Object.keys(Memory.poiDetails), (key) => Memory.poiDetails[key] == candidate);
            //console.log(JSON.stringify(candidate));
            //console.log(JSON.stringify(pos));
            console.log(this.evalfunc(candidate));
            if(this.evalfunc(candidate) <= 0) break;
            this.insertPoiBest(pos);
        }
    },
    
    run: function(){
        if(Game.time % 50 == 3)
            this.cleanupPOIs();
            
        if(Game.time % 5 == 3)
            this.updatePoiBest();
            
        // Draw POIs.
        for(var pos in Memory.poiDetails){
            pos = RoomPosition.deserialize(pos);
            var room = Game.rooms[pos.roomName];
            if(room == null) continue;
            room.visual.circle(pos, {radius: 0.6, fill: null, stroke: "#88ccdd", strokeWidth: 0.04, opacity: 0.3});
        }
        // Draw POIbest
        for(var pos in Memory.poiBest){
            pos = Memory.poiBest[pos];
            if(pos == null) continue;
            pos = RoomPosition.deserialize(pos);
            var room = Game.rooms[pos.roomName];
            if(room == null) continue;
            room.visual.circle(pos, {radius: 0.6, fill: null, stroke: "#88ffdd", strokeWidth: 0.08, opacity: 0.7});
        }
    }
};

module.exports = poi;