var manager = require('rooms.manager');

var trader = {
    run: function(){
        //if(Game.time % 17 != 9) return;
        
        manager.getControlledRooms().forEach((r) => {
            var terminal = Game.rooms[r].terminal;
            if(terminal == null) return;
            
            this.sellEnergy(terminal);
        });
    },
    
    sellEnergy: function(t){
        var amt = t.store.energy;
        var min_price = Math.max(0.05 - (amt / 5000000), 0.01);
        
        // Find available orders.
        var orders = Game.market.getAllOrders((o) => o.type == ORDER_BUY &&
                                                     o.resourceType == RESOURCE_ENERGY &&
                                                     o.price >= min_price);
        if(orders.length == 0) return;
        
        var energyPriceEvaluator = function(o){
            var inc = o.price * o.amount;
            var comission = Game.market.calcTransactionCost(o.amount, o.roomName, t.pos.roomName);
            var total_energy = o.amount + comission;
            return inc/total_energy;
        };
        
        // Get the best price w/ comission.
        var best = _.max(orders, energyPriceEvaluator);
        var real_price = energyPriceEvaluator(best);
        if(real_price < min_price) return;
        
        // Find the topmost amount that we can sell at this price.
        var storage_level = 5000000 * (0.05 - real_price);
        var can_feasibly_sell = amt - storage_level;
        
        // Compute cost ratio.
        var ratio = (1000000 + Game.market.calcTransactionCost(1000000, best.roomName, t.pos.roomName))/1000000;
        
        var we_have = Math.floor(0.95 * can_feasibly_sell / ratio);
        var can_sell = Math.min(we_have, best.amount);
        
        // Ignore small transactions.
        if(can_sell < 2000) return;
        
        console.log("Can sell energy!");
        console.log(JSON.stringify(best));
        console.log(" real price: " + real_price.toFixed(4) + " amt: " + amt + " ratio: " + ratio.toFixed(3) + " sell amt: " + can_sell + " storage level: " + storage_level + " total profit: " + (real_price * can_sell).toFixed(2));
        
        if(can_feasibly_sell <= 0){
            console.log("WARNING: can_feasibly_sell <=0 PLZ DEBUG, save this line and ~4 lines above!");
        }
        
        Game.market.deal(best.id, can_sell, t.pos.roomName);
    }
};

module.exports = trader;

