var utils = require('utils');
var manager = require('rooms.manager');
var containers = require('containers');
var gameMode = require('gamemode');

var CARRY_LIMIT = 20;

var HARD_LIMIT = 6;

var print = utils.print;

var INF = utils.INF;
var ERR = utils.ERR;
var VRB = utils.VRB;
var DBG = utils.DBG;

var upgradersController = {

    name: "Upgraders Controller",

    roleName: "upgrader",

    getDesiredWorkForce: function(ctrl){
        var lvl = utils.getControllerLevel(ctrl);
        // TODO: Finetune these numbers.
        // You are welcome to edit these! Just make sure to stay within reasonable values.
        // Also make sure this sequence is increasing.
        if(lvl < 2) return 4;  // Next level at 200
        if(lvl < 2.2) return 12;
        if(lvl < 2.5) return 20;
        if(lvl < 3) return 30; // Next level at 45'000
        if(lvl < 4) return 42; // Next level at 135'000
        if(lvl < 5) return 75; // Next level at 405'000
        return 100; // TODO: Higher levels
    },

    getPriorities: function(){
        return _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
    },

    prepare: function() {
        var controllers = _.map(manager.getControlledRooms(), (r)=>Game.rooms[r].controller);
        var desired = 0;
        var available = 0;
        var minRate = 1;
        var str = "Upgrader status: ";
        for(var ctrl in controllers){
            ctrl = controllers[ctrl];
            var a = this.getCurrentlyAvailableWorkForce(ctrl.room.name);
            var d = this.getDesiredWorkForce(ctrl);
            if(ctrl.room.storage != null){
                var s = ctrl.room.storage;
                var upgradeRatio = s.store[RESOURCE_ENERGY] / s.storeCapacity;
                d = Math.floor(d * Math.max(1, upgradeRatio*7));
            }

            available += a;
            desired += d;
            str += " [" + ctrl.room.name+ ": "+a+"/"+d+"] ";

            if(a/d <= minRate){
                minRate = a/d;
                this.targetControllerLevel = ctrl.level;
                this.targetRoomName = ctrl.room.name;
                var nextSpawn = manager.findNearestInAllRoomsFiltered(
                    FIND_STRUCTURES,
                    (x)=>x.structureType==STRUCTURE_SPAWN,
                    ctrl.pos);
                this.targetSpawnRoomName = nextSpawn.room.name;
            }
        }
        print(VRB, str + " min: " + minRate.toFixed(2) + " target: " + this.targetRoomName);
        if(this.creepsCount() >= HARD_LIMIT*controllers.length || gameMode.isBootstrap(this.targetRoomName)){
            this.priority = 0;
        }else if(minRate == 0){
            this.priority = 0.96; // There must be at least one creep upgrading, to prevent downgrading.
        }else if(minRate >= 1){
            this.priority = 0.1; // Extra creeps are always cool, but we don't need them too much.
        }else{
            this.priority = 0.99 - 0.89*(Math.min(1.0, minRate));
        }
    },

    getPriority: function(roomName) {
        if(this.priority == null){
            print(ERR, "Priority not calculated in " + this.name);
            return -0.01;
        }else{
            if(this.targetSpawnRoomName == null)
                return 0;
            if(roomName==null){
                roomName = this.targeSpawnRoomName;
            }
            var weight = Math.min(1.0/(0.15+Math.pow(Game.map.getRoomLinearDistance(roomName, this.targetSpawnRoomName), 1/4)), 1);
            if(isNaN(weight))
                weight = 1;
            return this.priority*weight;
        }
    },

    getEnergyThreshold: function(){
        return 1450;
    },

    getIdealCreep: function(energy, roomName) {
        if(gameMode.isUpgradeOnly(roomName)){
            var basicSet = [WORK, WORK, WORK, WORK, WORK, CARRY, MOVE];
            var baseName = "HU";
        }else if(this.targetControllerLevel>4){
            var basicSet = [WORK, WORK, WORK, WORK, MOVE];
            var baseName = "SU";
        }else{
            var basicSet = [WORK, WORK, CARRY, MOVE];
            var baseName = "U";
        }
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            utils.print(2, "Something went really wrong with cost caluclation!");
        }
        if(this.targetControllerLevel>4){
            energy -= utils.calculateSpawnCost([CARRY, CARRY]);
            var parts = [CARRY, CARRY];
        }else{
            var parts = [];
        }

        var level = Math.floor(energy/basicCost);
        if(level == 0){
            return null;
        }
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }

        var workParts = _.filter(parts, (part) => part == WORK).length;
        var creep = {
            parts: parts,
            name: baseName+level,
            memory: {
                role: this.roleName,
                upgrading: false,
                targetRoomName: this.targetRoomName,
                workForce: 2*workParts
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    getCurrentlyAvailableWorkForce: function(roomName){
        var total = 0;
        var cs = this.getCreeps();
        for(var creep in cs){
            creep = cs[creep];
            if(creep.memory.targetRoomName == roomName || (creep.memory.targetRoomName == null && creep.room.name == roomName ))
                total += creep.memory.workForce;
        }
        return total;
    },

    run: function(){
        var creeps = this.getCreeps();
        if(creeps.length>0)
            print(DBG, "RoomController:" + creeps[0].room.controller.progress + "/" + creeps[0].room.controller.progressTotal);
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
    },

    /** @param {Creep} creep **/
    move: function(creep) {
        if(creep.memory.targetRoomName == null){
            print(ERR, "Unassigned upgrader!");
            creep.memory.targetRoomName = manager.getControlledRooms()[0];
        }
        creep.say("Up "+ creep.memory.targetRoomName);
        if(creep.room.name != creep.memory.targetRoomName){
            if(creep.memory.get_energy!=null && creep.carry.energy < creep.carryCapacity){
                containers.creepGetEnergy(creep);
                return;
            }else{
                creep.moveToPOI(new RoomPosition(25, 25, creep.memory.targetRoomName));
                return;
            }
        }
        if(creep.memory.upgrading && creep.carry.energy <= creep.memory.workForce/2){
            creep.memory.upgrading = false;
            creep.say('harvesting');
        }
        if(!creep.memory.upgrading && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.upgrading = true;
            creep.say('upgrading');
        }

        if(creep.memory.upgrading){
            var ret = creep.upgradeController(creep.room.controller);
            if(ret == ERR_NOT_IN_RANGE){
                creep.moveToPOI(creep.room.controller);
            }
        }else{
            containers.creepGetEnergy(creep);
        }
    }
};

module.exports = upgradersController;
