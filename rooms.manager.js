var utils = require('utils');
var print = utils.print;
var INF = utils.INF;
var DBG = utils.DBG;
var ERR = utils.ERR;

function onlyUnique(value, index, self) {
    return _.findIndex(self, (x)=>(x.name == value.name)) == index
}

function scanPatch(x,y,room, d){
    var vals = room.lookAtArea(y-d, x-d, y+d, x+d, true);
    var ter = _.filter(vals, (x)=>x.type=='terrain' && x.terrain == 'wall');
    var str = _.filter(vals, (x)=>x.type=='structure');

    return ter.length + str.length;
}

function createSpawn(room, d){
    if(d == null){
        d = 6;
    }
    var bestPos = new RoomPosition(d+1,d+1, room.name);
    var bestScore = Math.pow(2*d+1, 2);
    var sources = room.find(FIND_SOURCES);
    for(var x=1+d; x <= 48-d; x++){
        for(var y=1+d; y <= 48-d; y++){
            var free = _.filter(room.lookAt(x,y), (x)=>(x.type=='terrain' && x.terrain == 'wall' ) || x.type=='structure');
            if(free.length>0){
                continue;
            }
            var score = scanPatch(x, y, room, 6);
            if(bestScore < score){
                continue;
            }
            var pos = new RoomPosition(x, y, room.name);
            for(var s in sources){
                s = sources[s];
                var path = room.findPath(s.pos, pos);
                score+=path.length;
            }
            if(bestScore > score){
                bestScore = score;
                bestPos.x = x;
                bestPos.y = y;
            }
        }
    }
    bestPos.createConstructionSite(STRUCTURE_SPAWN);
    return bestPos;
}

function getEuclideanDistance(room1, room2){
    var r1 = utils.parseRoomName(room1);
    var r2 = utils.parseRoomName(room2);
    var res = Math.sqrt(Math.pow(r1.X-r2.X, 2)+Math.pow(r1.Y-r2.Y, 2));
    print(res);
    return res;
}

function getDomain(room, rooms){
    var domain = [room];
    var trooms = [].concat(rooms);
    var a = true;
    while(a){
        a = false;
        trooms = _.remove(trooms,(r)=>domain.indexOf(r)==-1);
        var domainCore = _.filter(domain, (r)=>manager.getRoomStatus(r) == manager.OWN);
        if(domainCore == []){
            return domain;
        }
        var dist = _.map(trooms, (r)=>_.min(_.map(domainCore, (x)=>Game.map.getRoomLinearDistance(x,r))));
        var mindist = _.min(dist);
        if(mindist <= 1.01){
            a = true;
            domain.push(trooms[dist.indexOf(mindist)]);
        }
    }
    return domain;
}

function createDomains(rooms){
    var domains = [];
    var covered = [];
    for(var r in rooms){
        var room = rooms[r];
        var added = false;
        for(var d in domains){
            var domain = domains[d];
            if(domain.indexOf(room)!=-1){
                added = true;
            }
        }
        if(!added){
            var newDomain = getDomain(room, rooms);
            //newDomain = _.filter(newDomain, (x)=>covered.indexOf(x)!=-1);
            domains.push(newDomain);
            //covered.concat(newDomain);
        }
    }
    return domains;
}

function assignDomains(domains){
    if(Memory.domainsNumber == null){
        Memory.domainsNumber = 0;
    }
    print(DBG , "Assign domains:" + domains);
    for(var r in Memory.roomsData){
        var rn = Memory.roomsData[r].name;
        if(Memory.roomsData[r].domain == undefined){
            var res = _.map(domains, (d)=>d.indexOf(rn));
            var dNum = _.findIndex(res, (r)=>r!=-1);
            var domain = domains[dNum];
            var id = null;
            for(var d in domain){
                if(manager.getRoomDomainName(domain[d])!=null &&
                    Memory.roomsData[domain[d]] != null &&
                    Memory.roomsData[domain[d]].status==manager.OWN){
                    id = manager.getRoomDomainName(domain[d]);
                    break;
                }
            }
            if(Memory.roomsData[r].status==manager.OWN){
                if(id==null){
                    id = Memory.domainsNumber++;
                }
                for(var d in domain){
                    manager.setRoomDomain(domain[d], id);
                }
            }
        }
    }
}
var manager = {
    OWN: 0,
    FARM: 1,
    RESERVE: 2,
    CLAIM: 3,
    BLACKLIST: 255,

    initMemory: function(){
        if(Memory.roomsData != null)
            delete Memory.roomsData;
        Memory.roomsData = [];
    },

    makeRoomsUnique: function(){
        var l = Memory.roomsData.filter(onlyUnique);
        Memory.roomsData = l;
    },

    run: function(){
        if(Memory.roomsData == null || (Memory.rooms!=null && Memory.roomsData.length < Memory.rooms.length)){
            Memory.roomsData = [];
            for(var r in Memory.rooms){
                Memory.roomsData.push({name:Memory.rooms[r].name, status:Memory.rooms[r].interestLevel});
            }
        }
        if(Game.time % 23 == 21){
            Memory.roomsData = _.uniq(Memory.roomsData, (r)=>r.name);
            Memory.domainsNumber = 0;
            for(var r in Memory.roomsData){
                Memory.roomsData[r].domain =undefined;
            }
            var domains = createDomains(this.getControlledRooms().concat(this.getFarmRooms()));
            print(1, JSON.stringify(domains));
            assignDomains(domains);
        }
    },

    getRoomDomainName: function(roomName){
        return _.find(Memory.roomsData, (x)=>x.name==roomName).domain;
    },

    setRoomDomain: function(roomName, domain){
        var idx =  _.findIndex(Memory.roomsData, (x)=>x.name==roomName);
        if(idx==-1)return;
        Memory.roomsData[idx].domain = domain;
    },

    getDomains: function(){
        var dom = _.map(Memory.roomsData, (r)=>r.domain);
        return _.uniq(dom);
    },

    getRoomsInDomain: function(domain){
        var rooms = _.filter(Memory.roomsData, (r)=>r.domain==domain);
        return _.map(rooms, (r)=>r.name);
    },

    getRooms: function(){
        if(Memory.roomsData == null)
            Memory.roomsData = [];
        return _.map(Memory.roomsData, (x)=>x.name);
    },
    getRoomStatus: function(roomName){
        if(this.isUnknown(roomName ))return -1;
        return _.find(Memory.roomsData, (x)=>(x.name==roomName)).status;
    },

    isUnknown: function(roomName){
        return _.find(Memory.roomsData, (x)=>(x.name==roomName)) == undefined;
    },

    getControlledRooms: function(){
        var f = _.filter(Memory.roomsData, (x)=>x.status==this.OWN);
        return _.map(f, (x)=>x.name);
    },

    getFarmRooms: function(){
        var f = _.filter(Memory.roomsData, (x)=>x.status!=this.OWN);
        return _.map(f, (x)=>x.name);
    },



    getReservedRooms: function(){
        var f =  _.filter(Memory.roomsData, (x)=>x.status==this.RESERVE);
        return _.map(f, (x)=>x.name);
    },

    setStatus: function(roomName, roomStatus){
        if(roomName == null){
            utils.print(utils.ERR, "Cannot set status of null room!");
            return;
        }
        if(this.isUnknown(roomName)){
            print(utils.ERR, "Cannot set status of unknown room!");
        }else{
            var r = _.findIndex(Memory.roomsData, (x)=>x.name==roomName);
            // Workaround of bug in harvest
            if(roomStatus != this.FARM){
                Memory.roomsData[r].status = roomStatus;
            }
        }
    },

    getRoomsMaxEnergy: function(){
        return _.map(this.getControlledRooms(), (x)=>Game.rooms[x].energyCapacityAvailable);

    },

    getRoomsEnergy: function(){
        return _.map(this.getControlledRooms(), (x)=>Game.rooms[x].energyAvailable);

    },

    getCreepsInRoom:function(roomName, creepRole){
        var creeps = Game.creeps;
        return _.filter(creeps, (c)=>c.memory.role==creepRole && c.room.name == roomName);

    },

    getSpawnAvailable:function(){
        var rooms = this.getControlledRooms();
        var spawns = _.values(Game.spawns);
        return _.map(rooms, (r)=>(_.filter(spawns, (s)=>s.room.name == r && s.spawning == null)).length>0);
    },

    getClosestRoom:function(targetRoom, roomStatus){
        var rooms = _.map(_.filter(Memory.roomsData, (r)=>r.status==roomStatus),(x)=>x.name);
        var dst = _.map(rooms,(r)=>Game.map.getRoomLinearDistance(targetRoom, r));
        var mindst = _.min(dst);
        var mins = _.filter(rooms, (r)=>Game.map.getRoomLinearDistance(targetRoom, r)==mindst);
        if(mins.length == 1){
            return mins[0];
        }else{
            var tr = utils.parseRoomName(targetRoom);
            var coords = _.map(mins, (m)=>utils.parseRoomName(m));
            var diff = _.map(coords, (x)=>(Math.pow(x.X-tr.X, 2)+Math.pow(x.Y-tr.Y, 2)))
            return mins[diff.indexOf(_.min(diff))];
        }
    },

    findInAllRooms: function(find){
        var res = []
        for(var room in Memory.roomsData){
            room = Memory.roomsData[room];
            room = Game.rooms[room.name];
            if(room == null)
                continue; // Possibly notify manager about the problem.
            res = res.concat(room.find(find));
        }
        return res;
    },

    findStructures: function(structureType, rooms){
        if(rooms == null){
            rooms = this.getRooms();
        }
        var res = [];
        for(var r in rooms){
            var room = Game.rooms[rooms[r]];
            if(room == null)
                continue; // Possibly notify manager about the problem.
            res = res.concat(room.find(FIND_STRUCTURES, {filter:(s)=>s.structureType==structureType}));
        }
        return res;
    },

    findInAllRoomsFiltered: function(find, predicate){
        var res = [];
        for(var room in Memory.roomsData){
            room = Memory.roomsData[room];
            room = Game.rooms[room.name];
            if(room == null)
                continue; // Possibly notify manager about the problem.
            res = res.concat(room.find(find, {filter: predicate}));
        }
        return res;
    },

    findInDomainFiltered: function(domain, find, predicate){
        var res = [];
        var rooms = this.getRoomsInDomain(domain);
        for(var room in rooms){
            room = Game.rooms[rooms[room]];
            if(room == null)
                continue; // Possibly notify manager about the problem.
            res = res.concat(room.find(find, {filter: predicate}));
        }
        return res;
    },

    findNearestInAllRoomsFiltered: function(find, predicate, pos){
        var res = [];
        for(var room in Memory.roomsData){
            room = Memory.roomsData[room];
            room = Game.rooms[room.name];
            if(room == null)
                continue; // Possibly notify manager about the problem.
            res = res.concat(room.find(find, {filter: predicate}));
        }
        var best = _.min(res, (a) => pos.multiroomRangeTo(a.pos));
        return best;
    },

    initRoom: function(room, roomStatus){
        if(room == null){
            print(ERR, "Cannot intialize a null room!");
            return;
        }

        if(!this.isUnknown(room.name)  && roomStatus != this.OWN){
            print(INF, "Already known!");
            return;
        }
        /* Recalculate domains */
        //var domains = createDomains(this.getControlledRooms().concat(this.getFarmRooms()));
        //print(1, JSON.stringify(domains));
        //1assignDomains(domains);

        if(roomStatus == this.OWN){
            var sources = room.find(FIND_SOURCES);
            utils.buildAccompanyingContainer(room.controller.pos, "controller");

            if(sources.length < 4){
                var spawns = room.find(FIND_MY_STRUCTURES, {filter: (x)=>x.structureType == STRUCTURE_SPAWN});
                if(spawns.length>0){
                    utils.buildAccompanyingContainer(spawns[0].pos, "spawn");
                }else{
                    // TODO: Place spawn in a clever way.
                    var spawnPos = createSpawn(room, 6);
                    if(spawnPos == null)
                        print(ERR, "No spawn in owned room!");
                    else
                        utils.buildAccompanyingContainer(spawnPos, "spawn");

                }
            }
        }
        Memory.roomsData.push({name: room.name, status: roomStatus});
    }
};
module.exports = manager;

