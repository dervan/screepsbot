var utils = require('utils');
var roadStatistician = require('road.statistician');
var manager = require('rooms.manager');
var containers = require('containers');
var gameMode = require('gamemode');

var STATE_BUILDING = "building";
var STATE_HARVESTING = "harvesting";

// Do not spawn new road construction sites if there are already this many construction sites present.
var AUTO_ROAD_MAX_SITES = 4;

// This table specifies how many extensions are available at each room control level.
var EXT_AVAILABLE = {0: 0, 1: 0, 2: 5, 3: 10, 4: 20, 5: 30, 6: 40, 7: 50, 8: 60};

var HARD_LIMIT = 20;

var DBG = utils.DBG;
var INF = utils.INF;
var print = utils.print;

var buildersController = {

    name: "Builders Controller",

    roleName: "builder",

    getDesiredWorkForce: function(domain){
        var rooms = manager.getRoomsInDomain(domain);
        var force = 0;
        var levelForces = [10, 20, 50, 90, 140, 170, 210, 260, 200, 200];
        for(var r in rooms){
            var lvl = Math.floor(utils.getRoomLevel(rooms[r]));
            force += levelForces[lvl];
        }
        return force;
    },

    prepare: function(){
        this.domains = manager.getDomains();
        this.creeps = _.map(this.domains, (domain)=>manager.findInDomainFiltered(domain, FIND_MY_CREEPS, (x)=>x.memory.role == this.roleName));
        this.workForce = _.map(this.creeps, (cs)=>this.getCurrentlyAvailableWorkForce(cs));
        this.desiredForce =  _.map(this.domains, (d)=>this.getDesiredWorkForce(d));
        utils.print(utils.DBG,"Builder WorkForce: Available: " + JSON.stringify(this.workForce) + ", desired: " + JSON.stringify(this.desiredForce));

    },

    getDomainPriority: function(domain){
        var did = this.domains.indexOf(domain);
        var creepsCount = this.creeps[did].length;
        var available = this.workForce[did];
        var desired = this.desiredForce[did];
        if(creepsCount >= HARD_LIMIT || gameMode.isBootstrap(manager.getRoomsInDomain(domain)[0]) || desired == 0){
            return 0;
        }else if(available >= desired){
            return 0.11; // Extra builder creeps are always cool, but we don't need them too much.
        }else{
            return 0.89 - 0.69*(Math.min(0.89, available/desired));
        }
    },

    getPriorities: function(){
        var roomsDomains = _.map(manager.getControlledRooms(),(x)=>manager.getRoomDomainName(x));
        return _.map(roomsDomains, (d)=>this.getDomainPriority(d));
    },

    getPriority: function(roomName){
        var domain = manager.getRoomDomainName(roomName);
        return this.getDomainPriority(domain);
    },

    getEnergyThreshold: function(){
        return 1200;
    },

    getIdealCreep: function(energy, roomName) {
        if(utils.getRoomLevel(roomName) >= 3){
            var basicSet = [WORK, CARRY, MOVE];
            var baseName = "B";
        }else{
            var basicSet = [WORK, CARRY, MOVE];
            var baseName = "LB"; // Light Builder
        }
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            utils.print(2, "Something went really wrong with cost caluclation!");
        }
        var level = Math.floor(energy/basicCost);
        var parts = [];
        if(level == 0){
            return null;
        }
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }
        parts = parts.slice(0,50);
        var workParts = _.filter(parts, (part) => part == WORK).length;
        var creep = {
            parts: parts,
            name: baseName+level,
            memory: {
                role: this.roleName,
                target: null,
                state: STATE_HARVESTING,
                workForce: 5 * workParts
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    getCurrentlyAvailableWorkForce: function(cs){
        var total = 0;
        for(var creep in cs){
            creep = cs[creep];
            if(creep.memory.workForce == null || creep.memory.workForce == 0){ // TODO: Remove this condition in a next generation
                var workParts = _.filter(creep.body, (part) => part.type == WORK).length;
                creep.memory.workForce = 5 * workParts;
            }
            total += creep.memory.workForce;
        }
        return total;
    },

    buildRoads: function(room) {
        // Check if there are any construction sites.
        var csites = room.find(FIND_CONSTRUCTION_SITES);
        // If more than N objects are being build already, do not spawn new construction sites.
        if(csites.length >= AUTO_ROAD_MAX_SITES)
            return;
        // If any of these sites is an extension, focus on finishing it first.
        for(var csite in csites){
            if(csite.structureType == STRUCTURE_EXTENSION)
                return;
        }
        // We must have some builders ready to proceed.
        if(this.creepsCount() < 2) return;

        // Okay, ask the statistician about some nice place to build a new road.
        var roomPos = roadStatistician.getBest(room);
        if(roomPos === null) return;

        // Create a new road construction site.
        var res = roomPos.createConstructionSite(STRUCTURE_ROAD);
        print(DBG, "Creating a new road construction site at " + roomPos.x + " " + roomPos.y + ", result: " + res);
    },
    
    placeExtensionRoads: function(pos, max_range){
        for(var range = 1; range <= max_range + 1; range++){
            for(var y = -range; y <= range; y++){
            for(var x = -range; x <= range; x++){
                if((range > 1 && (x+y) % 2 == 0) || (pos.x + x)<=1 || (pos.x +x)>=48 || (pos.y + y)<=1 || (pos.y + y)>=48) continue;
                if(y == -range || y == range || x == -range || x == range){
                    var newpos = new RoomPosition(pos.x + x, pos.y + y, pos.roomName);
                    // Check if there are no creeps there!
                    var creeps = newpos.lookFor(LOOK_CREEPS);
                    if(creeps.length > 0) continue;
                    // Check if suitable placement for a road
                    var here = newpos.look();
                    here = _.filter(here, (q) => {
                        if(q.type == 'terrain' && q.terrain == 'wall') return true;
                        if(q.type == 'structure' && q.structure.structureType == STRUCTURE_ROAD) return true;
                        return false;
                    });
                    if(here.length > 0) continue;
                    // Place road here.
                    var res = newpos.createConstructionSite(STRUCTURE_ROAD);
                    if(res == OK){
                        console.log("Placed some roads around spawn extensions.");
                    }
                }
            }
            }
        }
    },

    placeExtensionCS: function(pos){
        var MAX_RANGE = 16;
        for(var range = 2; range <= MAX_RANGE; range++){
            for(var y = -range; y <= range; y++){
            for(var x = -range; x <= range; x++){
                if((x+y) % 2 != 0 || (pos.x + x)<=1 || (pos.x +x)>=48 || (pos.y + y)<=1 || (pos.y + y)>=48) continue;
                if(y == -range || y == range || x == -range || x == range){
                    var newpos = new RoomPosition(pos.x + x, pos.y + y, pos.roomName);
                    // Check if there are no creeps there!
                    var creeps = newpos.lookFor(LOOK_CREEPS);
                    if(creeps.length > 0) continue;
                    // Try placing the CS here.
                    var res = newpos.createConstructionSite(STRUCTURE_EXTENSION);
                    if(res == OK){
                        print(INF, "Placed new extension CS at " + x + " " + y);
                        this.placeExtensionRoads(pos, range);
                        return;
                    }
                }
            }
            }
        }
    },

    buildExtensions: function(spawn) {
        if(Game.time % 5 != 0) return;
        var room = Game.rooms[spawn.pos.roomName];
        var ext_available = EXT_AVAILABLE[room.controller.level];

        var ext_at_prev = EXT_AVAILABLE[room.controller.level - 1];
        var new_ext = ext_available - ext_at_prev;
        var f = room.controller.progress / room.controller.progressTotal;
        var ext_desired = ext_at_prev + Math.ceil(f * new_ext);

        utils.print(utils.DBG, "Desired extensions: " + ext_desired + " " + f + " " + ext_at_prev);

        var ext_csites = room.find(FIND_CONSTRUCTION_SITES, {filter: {structureType: STRUCTURE_EXTENSION}});
        if(ext_csites.length > 0) return; // No more than 1 EXT CS at a time.

        var exts = room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_EXTENSION}});

        var ext_free = ext_desired - exts.length;
        if(ext_free <= 0) return;
        print(INF, "We may still build " + ext_free + " extensions.");

        this.placeExtensionCS(spawn.pos);
    },

    run: function(roomName){
        var room = Game.rooms[roomName];
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
        if(Memory.wall_hits_max == null){
            Memory.wall_hits_max = 1000;
        }
        // Temporarily disabled to investige no-roads efficiency.
        // Per each room we want to automatically build roads in:
        // this.buildRoads(room);
        var rooms = manager.getControlledRooms();
        rooms.forEach((room) => {
            room = Game.rooms[room];
            var towers = room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
            towers.forEach((tower) => {
                var nearby = tower.pos.findInRange(FIND_STRUCTURES, 16);
                for(var s in nearby){
                    var s = nearby[s];
                    if(((s.structureType == STRUCTURE_WALL || s.structureType == STRUCTURE_RAMPART) &&
                        s.hits<Memory.wall_hits_max)){
                        tower.repair(s);
                        continue;
                    }else if((s.structureType != STRUCTURE_WALL && s.structureType != STRUCTURE_RAMPART) &&
                        s.hits <= s.hitsMax-800 && tower.pos.getRangeTo(s)<8){
                        tower.repair(s);
                    }
                }
            });
        });
        var spawns = Game.spawns;
        for(var s in spawns){
            this.buildExtensions(spawns[s]);
        }

        this.rebuildRepairablesLists();
        
        this.displayRepairables();
    },
    
    displayRepairables: function(){
        for(var r in Memory.repairables){
            r = Memory.repairables[r];
            var room = Game.rooms[r.pos.roomName];
            if(room == null) continue;
            var color = "white";
            if(r.priority == this.REPAIR_URGENTLY) color = "red";
            if(r.priority == this.REPAIR_WHEN_CONVENIENT) color = "orange";
            room.visual.circle(r.pos.x + 0.2, r.pos.y - 0.2, {fill: color});
        }
    },

    REPAIR_WHEN_CONVENIENT: 1,
    REPAIR_URGENTLY: 2,

    rebuildRepairablesLists: function(){
        if(Game.time % 20 != 16) return;
        Memory.repairables = [];
        var allStructures = manager.findInAllRooms(FIND_STRUCTURES);
        for(var s in allStructures){
            s = allStructures[s];
            if(s.hitsMax == 0){
                continue;
            }
            if(s.structureType == STRUCTURE_WALL || s.structureType == STRUCTURE_RAMPART){
                var p = s.hits/Memory.wall_hits_max;
            }else{
                var p = s.hits/s.hitsMax;
            }
            var priority = 0;
            if(p < 0.85) priority = this.REPAIR_WHEN_CONVENIENT;
            if(p < 0.4) priority = this.REPAIR_URGENTLY;
            if(priority == 0) continue;
            var repairable = {
                id: s.id,
                priority: priority,
                type: s.structureType,
                pos: s.pos
            };
            Memory.repairables.push(repairable);
        }
        var allcnt = Memory.repairables.length;
        var urgentcnt = _.filter(Memory.repairables, (r) => r.priority >= this.REPAIR_URGENTLY).length;
        print(INF, "Repairables list re-created, found " + allcnt + " repairables, " + urgentcnt + " of them are urgent.");
    },

    chooseRepairTarget: function(pos, minimal_urgency){
        //console.log("Searching for repair target with MU " + minimal_urgency + " from " + pos);
        var matches = _.filter(Memory.repairables, (r) => r.priority >= minimal_urgency);
        matches = _.filter(matches, (r) => !this.isStructureBeingRepaired(r.id));
        if(matches.length == 0)
            return null;
        var nearest = _.min(matches, function(r){
            var rpos = new RoomPosition(r.pos.x, r.pos.y, r.pos.roomName);
            return rpos.multiroomRangeTo(pos);
        });
        //console.log("Nearest is " + JSON.stringify(nearest.pos));
        return nearest.id;
    },

    isStructureBeingRepaired: function(structure_id){
        return _.filter(Game.creeps, (c) => c.memory.role == "builder" && c.memory.repair_target == structure_id).length > 0;
    },

    chooseBuildTargetByPrio: function(pos){
        var csites = manager.findInAllRooms(FIND_MY_CONSTRUCTION_SITES);

        var csites_extensions = _.filter(csites, (site) => site.structureType == STRUCTURE_EXTENSION);
        var csites_roads = _.filter(csites, (site) => site.structureType == STRUCTURE_ROAD);
        var csites_other = _.filter(csites, (site) => site.structureType != STRUCTURE_ROAD && site.structureType != STRUCTURE_EXTENSION);

        if(csites_other.length > 0){
            // First, focus on custom structures. Start with the one that will take the least time to build.
            return _.min(csites_other, (csite) => csite.progressTotal - csite.progress);
        }else if(csites_extensions.length > 0){
            // Then build extensions.
            return _.min(csites_extensions, (csite) => csite.progressTotal - csite.progress);
        }else if(csites_roads.length > 0){
            // Finally build roads, starting from the ones nearby.
            return _.min(csites_roads, (csite) => csite.pos.multiroomRangeTo(pos));
        }
        return null;
    },

    getRepairableRoadsInRange: function(pos){
        return pos.findInRange(FIND_STRUCTURES,3,{filter: (s) => s.structureType == STRUCTURE_ROAD && s.hits < s.hitsMax});
    },

    idle: function(creep) {
        // Called when the creep has nothing to do.
        var target = this.chooseRepairTarget(creep.pos, this.REPAIR_WHEN_CONVENIENT);
        if(target != null){
            creep.memory.repair_target = target;
            creep.memory.repair_urgent = false;
            var tobj = Game.getObjectById(target);
            if(tobj != null)
                print(DBG, "Creep " + creep.name + " has nothing to do and therefore will repair " + target + " at " + JSON.stringify(tobj.pos));
            return this.doRepair(creep);
        }

        // Nothing to do. Park.
        utils.creepMoveToRandom(creep);
    },

    /** @param {Creep} creep **/
    move: function(creep) {
        var dropped = creep.pos.findInRange(FIND_DROPPED_ENERGY, 1);
        if(dropped != null && creep.carry.energy < creep.carryCapacity){
            creep.pickup(dropped[0]);
        }
        if(creep.memory.state === undefined)
            creep.memory.state = STATE_HARVESTING;

        if(creep.memory.state == STATE_BUILDING && creep.carry.energy === 0) {
            creep.memory.state = STATE_HARVESTING;
            creep.memory.target = null;
            creep.say('gathering');
        }
        else if(creep.memory.state == STATE_HARVESTING && creep.carry.energy === creep.carryCapacity) {
            creep.memory.state = STATE_BUILDING;
            creep.memory.source = null;
            creep.say('building');
        }
        
        // Draw visual line.
        var target = creep.memory.repair_target;
        if(target != null){
            target = Game.getObjectById(creep.memory.repair_target);
            if(target != null)
                utils.multiroomLine(target.pos, creep.pos, {color: 'gold', opacity: 0.2});
        }else if(creep.memory.target != null){
            target = Game.getObjectById(creep.memory.target);
            if(target != null)
                utils.multiroomLine(target.pos, creep.pos, {color: 'gold', opacity: 0.2});
        }

        if(creep.memory.state == STATE_BUILDING) {
            // Is this creep locked in repairing sub-mode?
            if(creep.memory.repair_target != null && creep.memory.repair_urgent)
                return this.doRepair(creep);

            // Is there anything that needs urgent repairs?
            var target = this.chooseRepairTarget(creep.pos, this.REPAIR_URGENTLY);
            if(target != null){
                print(DBG, "Setting target");
                creep.memory.repair_target = target;
                creep.memory.repair_urgent = true;
                return this.doRepair(creep);
            }
            
            // Is this creep locked in repairing sub-mode?
            if(creep.memory.repair_target != null)
                return this.doRepair(creep);

            // If not, then proceed to building new structures.
            var target = this.chooseBuildTargetByPrio(creep.pos);
            if(target == null)
                return this.idle(creep);
            creep.memory.target = target.id;
            var res = creep.build(target);
            if(res == ERR_NOT_IN_RANGE)
                this.moveWithRepair(creep,target)

        } else {
            containers.creepGetEnergy(creep);
        }
    },

    /* Moves the creep, while reparing roads nearby. */
    moveWithRepair: function(creep, target){
        var repairable = this.getRepairableRoadsInRange(creep.pos);
        if(repairable.length > 0){
            var r = _.sample(repairable);
            creep.repair(r);
        }
        creep.moveTo(target);
    },

    doRepair: function(creep){
        var target = Game.getObjectById(creep.memory.repair_target);
        if(target == null){
            // Target ceased to exist or we have an invalid reference
            delete creep.memory.repair_target;
            delete creep.memory.repair_urgent;
        }else if (target.hits == target.hitsMax ||
                  ((target.structureType == STRUCTURE_WALL ||
                    target.structureType == STRUCTURE_RAMPART) &&
                   target.hits>=Memory.wall_hits_max)){
            print(DBG, "Creep " + creep.name + " finished repairing " + creep.memory.repair_target + " " + target.pos);
            delete creep.memory.repair_target;
            delete creep.memory.repair_urgent;
        }else{
            //console.log("Creep " + creep.name + " does repairs to " + creep.memory.repair_target);
            var res = creep.repair(target);
            if(res == ERR_NOT_IN_RANGE){
                this.moveWithRepair(creep, target);
            }else if(res == ERR_INVALID_TARGET){
                print(ERR, "Invalid repair target " + target + "!");
                creep.memory.target = undefined;
            }
        }
    }
};

module.exports = buildersController;
