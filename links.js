var utils = require('utils');
var manager = require('rooms.manager');

var links = {
    run: function(){
        if(!Memory.linkData)
            Memory.linkData = {}
        if(Game.time % 23 == 1)
            this.rebuildLinkLists();
        this.transferAll();
    },
    
    rebuildLinkLists: function(){
        console.log("Rebuilding link lists.");
        Memory.linkData = {};
        var rooms = manager.getControlledRooms();
        for(var room in rooms){
            room = Game.rooms[rooms[room]]
            var linklist = [];
            
            var links = room.find(FIND_MY_STRUCTURES, {filter: (s) => s.structureType==STRUCTURE_LINK});
            for(var i in links){
                var link = links[i];
                // Does this link has a storage nearby?
                var stos = link.pos.findInRange(FIND_MY_STRUCTURES, 3, {filter: (s) => s.structureType == STRUCTURE_STORAGE});
                var hasStorageNearby = stos.length > 0;
                
                // Does this link has a controller nearby?
                var hasControllerNearby = link.room.controller.pos.getRangeTo(link.pos) <= 4;
                
                linklist.push({
                    id: link.id,
                    isCentral: hasStorageNearby,
                    suppliesController: hasControllerNearby
                });
            }
            
            Memory.linkData[room.name] = linklist;
        }
    },
    
    getCentralLinkForRoom: function(roomName){
        if(Memory.linkData[roomName] == null) return null;
        var clist = _.filter(Memory.linkData[roomName], (l) => l.isCentral);
        if(clist.length < 1) return null;
        return Game.getObjectById(clist[0].id);
    },
    
    
    getControllerLinksForRoom: function(roomName){
        if(Memory.linkData[roomName] == null) return null;
        return _.map(_.filter(Memory.linkData[roomName], (l) => l.suppliesController), (l) => Game.getObjectById(l.id));
    },
    
    getNonCentralLinksForRoom: function(roomName){
        if(Memory.linkData[roomName] == null) return null;
        return _.filter(Memory.linkData[roomName], (l) => !l.isCentral);
    },
    
    getAllLinksForRoom: function(roomName){
        if(Memory.linkData[roomName] == null) return [];
        return Memory.linkData[roomName];
    },
    
    transferAll: function(){
        for(var room in Memory.linkData){
            var storagelink = this.getCentralLinkForRoom(room);
            var controllerlinks = this.getControllerLinksForRoom(room);
            
            let clink = storagelink;
            
            if(clink == null) continue;
            this.getNonCentralLinksForRoom(room).forEach((l) => {
                var link = Game.getObjectById(l.id);
                if(link.energy < 0.5 * link.energyCapacity) // Don't bother.
                    return;
                // Push energy to the central link.
                link.transferEnergy(clink);
            });
            
            
        }
    }
};

module.exports = links;
