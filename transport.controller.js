var utils = require('utils');
var manager = require('rooms.manager');
var containers = require('containers');
var gameMode = require('gamemode');
var links = require('links');

function print(a,b){
    utils.print(a,b);
}
var DBG = 5;
var ERR = 1;
var HARD_LIMIT = 20;

function hubIsFull(hubObj){
    if(hubObj == null) return false;
    if(hubObj.structureType == STRUCTURE_LINK) return false;
    if(hubObj.structureType == STRUCTURE_EXTENSION ||
        hubObj.structureType == STRUCTURE_SPAWN ||
        hubObj.structureType == STRUCTURE_TOWER)
        return hubObj.energy == hubObj.energyCapacity;
    else
        return hubObj.store[RESOURCE_ENERGY] == hubObj.storeCapacity;
}

function leafIsEmpty(hubObj){
    if(hubObj.structureType == STRUCTURE_EXTENSION ||
        hubObj.structureType == STRUCTURE_SPAWN ||
        hubObj.structureType == STRUCTURE_TOWER)
        return hubObj.energy == 0;
    else
        return hubObj.store[RESOURCE_ENERGY] == 0;
}

function canContainEnergy(s){
    return (s.structureType == STRUCTURE_EXTENSION ||
            s.structureType == STRUCTURE_CONTAINER ||
            s.structureType == STRUCTURE_STORAGE ||
            s.structureType == STRUCTURE_SPAWN ||
            s.structureType == STRUCTURE_TOWER ||
            s.structureType == STRUCTURE_LINK);

}

function isTargetNearLink(pos){
    var room = pos.roomName;
    var clink = links.getCentralLinkForRoom(room);
    if(clink != null && clink.pos.multiroomRangeTo(pos) < 10)
        return true;
        
    // Now, test for controller links.
    var clinks = links.getControllerLinksForRoom(room);
    for(var c in clinks){
        var link = Game.getObjectById(clinks[c].id);
        if(!link) continue;
        if(link.pos.multiroomRangeTo(pos) < 6) return true;
    }
    return false; // default
};

function considerLinks(creep, choice){
    // This function updates the choice to use links, if that would be beneficial.
    if(!isTargetNearLink(choice.meta.pos)) return choice;
    var room = choice.meta.pos.roomName;
    console.log("Creep " + creep.name + " considers to use link to room " + room);
    
    var choicePos = new RoomPosition(choice.meta.pos.x, choice.meta.pos.y, choice.meta.pos.roomName);
    var distToChoice = PathFinder.search(creep.pos, choicePos).path.length;
    
    var linklist = links.getAllLinksForRoom(room)
    var minDistToLink = distToChoice - 10;
    for(var i in linklist){
        var link = linklist[i];
        var linkObj = Game.getObjectById(link.id);
        // Unfortunatelly, we need to run A* here. But links lists are always short, and hub selection happens rarely.
        var distToLink = PathFinder.search(creep.pos,linkObj.pos).path.length;
        //console.log(" " + choicePos.serialize() + " distToChoice " + distToChoice);
        //console.log(" " + linkObj.pos.serialize() + " distToLink " + distToLink);
        if(distToLink < minDistToLink){
            console.log("Using link at " + linkObj.pos.serialize() + " instead!");
            choice = {
                meta: {pos: linkObj.pos},
                obj: linkObj
            };
            minDistToLink = distToLink;
        }
    }
    
    return choice;
}

// Choose where to carry energy
function findHub(creep){
    var storages = containers.getContainersSupplyingEnergy();
    if(gameMode.isUpgradeOnly(creep.room.name)){
        // Do not put energy back into storage!
        storages = _.filter(storages, (s) => !s.isStorage);
    }
    storages = _.map(storages, (s) => {
        s.pos = new RoomPosition(s.pos.x, s.pos.y, s.pos.roomName);
        return {meta: s, obj: Game.getObjectById(s.id)};
    });
    storages = _.filter(storages, (s) => s.obj != null &&
        ((s.obj.structureType == STRUCTURE_TOWER && s.obj.my && s.obj.energy<s.obj.energyCapacity) ||
         (s.obj.structureType != STRUCTURE_TOWER && s.obj.store[RESOURCE_ENERGY] < s.obj.storeCapacity)));
    if(storages.length == 0){
        print(DBG, creep.name + ": No hub found!");
        return null;
    }
    var cost_function = function(s){
        // NOTE: This only works correctly if actual distance is used instead of range - but the pathfinding cost would be far too much!
        var distance = creep.pos.multiroomRangeTo(s.meta.pos);
        var bonus = Math.log10(Math.max(distance,5))*300;
        var base = containers.getEnergyWithReservations(s.obj);
        // Prioritize empty containers.
        if(leafIsEmpty(s.obj)) bonus = -600;
        //console.log(creep.pos.serialize() + " " + s.meta.pos.serialize());
        //console.log("Distance: " + distance + ", base: " + base + ", bonus: " + bonus + ", total: " + (base+bonus));
        return base + bonus;
    };
    var maxcnt = _.min(storages, cost_function);
    var min = cost_function(maxcnt);
    storages = _.filter(storages, (s) => cost_function(s) == min);
    // When multiple containers seem equally good, choose the nearest one.
    var choice = _.min(storages, (s) => creep.pos.findPathTo(s.meta.pos).length);
    //utils.print(utils.INF,"findHub choice for " + creep.name + creep.pos + " : " + JSON.stringify(choice.meta.pos));
    choice = considerLinks(creep, choice);
    return {
        pos: choice.meta.pos,
        id: choice.obj.id
    };
}

// Choose where to take energy from
function findLeaf(creep){
    var storages = containers.getContainersGeneratingEnergy();
    if(gameMode.isUpgradeOnly(creep.room.name)){
        // In upgrade-only mode, harvesters are disabled, and we aim to empty the storage.
        var storages = storages.concat(containers.getContainerStorages());
    }
    storages = _.map(storages, (s) => {
        s.pos = new RoomPosition(s.pos.x, s.pos.y, s.pos.roomName);
        return {meta: s, obj: Game.getObjectById(s.id)};
    });
    //storages = _.filter(storages, (s) => s.obj != null && s.obj.store[RESOURCE_ENERGY] > 0);
    if(!gameMode.isUpgradeOnly(creep.room.name)){
        // Do not steal from containers that generate, but are also used as suppliers.
        // Ignore this rule in upgrade-only mode, though, as we wish to empty storage ASAP.
        storages = _.filter(storages, (s) => !(s.meta.suppliesSpawn || s.meta.suppliesController));
    }
    if(storages.length == 0){
        console.log(creep.name + ": No leaf found!");
        return null;
    }
    var cost_function = function(s){
        // NOTE: This only works correctly if actual distance is used instead of range - but the pathfinding cost would be far too much!
        var distance = creep.pos.multiroomRangeTo(s.meta.pos);
        var bonus = -Math.log10(Math.max(distance,5))*300;
        var base = containers.getEnergyWithReservations(s.obj);
        // Prioritize full containers.
        if(hubIsFull(s.obj)) bonus = 600;
        //console.log(creep.pos.serialize() + " " + s.meta.pos.serialize());
        //console.log("Distance: " + distance + ", base: " + base + ", bonus: " + bonus + ", total: " + (base+bonus));
        return base + bonus;
    };
    var maxcnt = _.max(storages, cost_function);
    var max = cost_function(maxcnt);
    storages = _.filter(storages, (s) => cost_function(s) == max);
    // When multiple containers seem equally good, choose the nearest one.
    var choice = _.min(storages, (s) => creep.pos.findPathTo(s.meta.pos).length);
    //utils.print(utils.INF, "findLeaf choice for " + creep.name + creep.pos + " : " + JSON.stringify(choice.meta.pos) + "energy " + max);

    return {
        pos: choice.meta.pos,
        id: choice.obj.id
    };
}


var transportController = {

    name: "Transporters Controller",

    roleName: "transporter",

    getDesiredCarryForce: function(domain){
        var contCount = manager.findInDomainFiltered(domain, FIND_STRUCTURES,(x)=>x.structureType == STRUCTURE_CONTAINER || x.structureType == STRUCTURE_STORAGE || x.structureType == STRUCTURE_TOWER).length;
        var factor = 1000;
        // TODO: Finetune these numbers.
        // You are welcome to edit these! Just make sure to stay within reasonable values.
        // Also make sure this sequence is increasing.
        return contCount * 0.8 * factor;
    },

    prepare: function() {
        this.domains = manager.getDomains();
        this.creeps = _.map(this.domains, (domain)=>manager.findInDomainFiltered(domain, FIND_MY_CREEPS, (x)=>x.memory.role == this.roleName));
        this.workForce = _.map(this.creeps, (cs)=>this.getCurrentlyAvailableCarryForce(cs));
        this.desiredForce =  _.map(this.domains, (d)=>this.getDesiredCarryForce(d));

        utils.print(utils.DBG,"Transporter CarryForce: Available: " + JSON.stringify(this.workForce) + ", desired: " + JSON.stringify(this.desiredForce));

    },

    getPriorities: function(){
        var roomsDomains = _.map(manager.getControlledRooms(),(x)=>manager.getRoomDomainName(x));
        return _.map(roomsDomains, (d)=>this.getDomainPriority(d));
    },

    getDomainPriority: function(domain) {
        var did = this.domains.indexOf(domain);
        var creepsCount = this.creeps[did].length;
        var available = this.workForce[did];
        var desired = this.desiredForce[did];
        if(creepsCount >= HARD_LIMIT*manager.getRoomsInDomain(domain).length || gameMode.isBootstrap(manager.getRoomsInDomain(domain)[0])){
            return 0;
        }else if(available >= desired){
            return 0; // Too many transporters bring no benefit.
        }else{
            return 1.0 - 0.3*(Math.min(1.0, available/desired));
        }
    },

    getPriority: function(roomName){
        var domain = manager.getRoomDomainName(roomName);
        return this.getDomainPriority(domain);
    },

    getEnergyThreshold: function(){
        return 8*150;
    },

    MAX_LEVEL: 8,
    getIdealCreep: function(energy, roomName) {
        if(utils.getRoomLevel(roomName) >= 3){
            var basicSet = [CARRY, CARRY, MOVE];
            var baseName = "T";
        }else{
            var basicSet = [CARRY, MOVE];
            var baseName = "LT"; // Light Transoprter
        }
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            utils.print(2, "Something went really wrong with cost caluclation!");
        }
        var level = Math.floor(energy/basicCost);
        if(level == 0) return null;
        level = Math.min(level, this.MAX_LEVEL); // Do not create unnecessarily large creeps.

        var parts = []
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }

        var carryParts = _.filter(parts, (part) => part == CARRY).length;
        var transportCreep = {
            parts: parts,
            name: baseName+level,
            memory: {
                carry_energy: false,
                role: this.roleName,
                leaf: {},
                hub: {},
                carryForce: 50*carryParts
            }
        };
        return transportCreep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    getCurrentlyAvailableCarryForce: function(cs){
        var total = 0;
        for(var creep in cs){
            creep = cs[creep];
            if(creep.memory.carryForce == null){ // TODO: Remove this condition in a next generation
                creep.memory.carryForce = creep.carryCapacity;
            }
            total += creep.memory.carryForce;
        }
        return total;
    },

    run: function(){
        var room = _.values(Game.rooms)[0];
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            if(creeps[i].spawning) continue;
            this.move(creeps[i]);
        }
    },

    /** @param {Creep} creep **/
    move: function(creep){
        var dropped = creep.pos.findInRange(FIND_DROPPED_ENERGY, 1);
        if(dropped != null && creep.carry.energy < creep.carryCapacity){
            creep.pickup(dropped[0]);
        }
        // Temporary fix to stop transporters from blocking everything when no target hub is present yet
        if(Memory.containerData.length <= 1)
            return utils.creepMoveToRandom(creep);

        if(creep.memory.carry_energy && creep.carry.energy < 0.4 * creep.carryCapacity) {
            creep.memory.carry_energy = false;
            creep.memory.leaf = findLeaf(creep);
            creep.say('Get');
        }else if(!creep.memory.carry_energy && creep.carry.energy > 0.4 * creep.carryCapacity) {
            creep.memory.hub = findHub(creep);
            creep.say('Carry');
            creep.memory.carry_energy = true;
        }

        if(creep.memory.carry_energy) {
            var hub = creep.memory.hub;
            if(hub == null || hub.pos == null){
                //print(4, "Null hub for transporter "+creep.name);
                creep.memory.hub = findHub(creep);
                hub = creep.memory.hub;
                if(creep.memory.hub == null){
                    utils.creepMoveToRandom(creep);
                    return;
                }
            }else{
                var hubPos = new RoomPosition(hub.pos.x, hub.pos.y, hub.pos.roomName);
                if(Game.rooms[hub.pos.roomName] == null){
                    /* Room unaccessible - creep is in other room! */
                    creep.moveToPOI(hubPos, {reusePath: 20});
                }else{
                    var hubObj = _.filter(hubPos.lookFor(LOOK_STRUCTURES), canContainEnergy)[0];
                    if(hubIsFull(hubObj)){
                        creep.memory.hub = findHub(creep);
                        return this.move(creep);
                    }
                    var ret = creep.transfer(hubObj, RESOURCE_ENERGY);
                    if(ret == ERR_NOT_IN_RANGE){
                        creep.moveToPOI(hubObj, {reusePath: 20});
                    }else if(ret == ERR_FULL){
                        creep.memory.hub = findHub(creep);
                    }else if(ret == OK){
                        creep.memory.hub = null;
                    }
                }

                var linePos = hub.pos;
                var lineColor = 'red';
            }
        }
        else
        {

            var leaf = creep.memory.leaf;
            if(leaf == null || leaf.pos == null){
                print(DBG, "Null leaf for transporter "+creep.name);
                creep.memory.leaf = findLeaf(creep);
                leaf = creep.memory.leaf;
                if(leaf == null){
                    utils.creepMoveToRandom(creep);
                    return;
                }
            }

            var leafPos = new RoomPosition(leaf.pos.x, leaf.pos.y, leaf.pos.roomName);
            if(Game.rooms[leaf.pos.roomName] == null){
                /* Room unaccessible - creep is in other room! */
                creep.moveToPOI(leafPos, {reusePath: 20});
            }else{
                var leafObj = _.filter(leafPos.lookFor(LOOK_STRUCTURES), canContainEnergy)[0];
                /*if(leafIsEmpty(leafObj)){
                    creep.memory.leaf = findLeaf(creep);
                    return this.move(creep);
                }*/
                var ret = creep.withdraw(leafObj, RESOURCE_ENERGY);
                if(ret == ERR_NOT_IN_RANGE){
                    creep.moveToPOI(leafObj, {reusePath: 20});
                }else if(ret != OK){
                    creep.memory.leaf = findLeaf(creep);
                }else if(ret == OK){
                    creep.memory.leaf = null;
                }

                var linePos = leaf.pos;
                var lineColor = 'green';
            }
        }
        if(linePos != null)
            utils.multiroomLine(linePos, creep.pos, {color: lineColor, width: 0.1});
    }
};

module.exports = transportController;
