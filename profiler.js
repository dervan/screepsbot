// Low-pass filter convex coefficient
var ALPHA = 0.04;
// With alpha = 0.04, the influence of measurement after 50 ticks is 13%, and after 100 ticks it is 1.6%.

var profiler = {
    // Must be called at the beginning of a tick.
    init: function(){
        if(Memory.profiler == null)
            Memory.profiler = {
                in_progress: {},
                this_tick: {},
                history: {}
            };
        Memory.profiler.this_tick = {};
    },
    // In order to yield meaningful results, measured sections must not overlap!
    measure: function(name, func){
        this.start(name);
        func();
        this.stop(name);
    },
    start: function(name){
        Memory.profiler.in_progress[name] = Game.cpu.getUsed();
    },
    stop: function(name){
        let start = Memory.profiler.in_progress[name];
        delete Memory.profiler.in_progress[name];
        let end = Game.cpu.getUsed();
        let time = end - start;
        Memory.profiler.this_tick[name] = time;
    },
    // Must be called at the end of a tick.
    finalize: function(){
        for(var name in Memory.profiler.this_tick){
            var time = Memory.profiler.this_tick[name];
            let old = Memory.profiler.history[name];
            if(old == null){
                Memory.profiler.history[name] = time;
            }else{
                // Low-pass filter
                let q = (ALPHA) * time + (1.0 - ALPHA) * old;
                Memory.profiler.history[name] = q;
            }
        }
        if(Game.time % 10 == 0){
            // Display statistics.
            let stats = [];
            for(var name in Memory.profiler.history){
                var time = Memory.profiler.history[name];
                stats.push({name: name, time:time});
            }
            stats = stats.sort((a,b) => b.time - a.time);
            
            let str = "";
            stats.forEach((q) => {
                str += "[" + q.name + ": " + q.time.toFixed(1) + "] ";
            })
            console.log("CPU STATS: " + str);
        }
    }
};

module.exports = profiler;