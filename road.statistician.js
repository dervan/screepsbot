var manager = require('rooms.manager');

var ROOM_SIZE = 50;

var ROOM_STAT_ROTATE_PERIOD = 1000;

var roadStatistician = {
    
    init: function() {
        if(Memory.roadStats === undefined || Memory.roadStats === null)
            this.clearAll();
    },
    
    initRoom: function(roomName){
        console.log("Initing room " + roomName);
        var a = new Array(ROOM_SIZE * ROOM_SIZE);
        for(var y_ = 0; y_ < ROOM_SIZE; y_++)
            for(var x_ = 0; x_ < ROOM_SIZE; x_++)
                // IMPORTANT: Memory is serialized to a string, so it is VERY important to keep this structure as small as possible, with super short field names!
                a[y_*ROOM_SIZE + x_] = {
                    v: 0, // Visit count
                    c: "", // Last creep
                    t: 0 // Game tick of last creep
                }
        Memory.roadStats.current[roomName] = a;
    },
    
    clearAll: function() {
        console.log("Clearing room stats.")
        Memory.roadStats = {
            ticksRecorded: 0,
            prev: {},
            current: {}
        };
    },
    
    getUsageCoeff: function(pos){
        var terrain = pos.lookFor(LOOK_TERRAIN)[0];
        if(terrain == "swamp")
            return 10; // TODO: Test if this shouldn't be 5.
        return 1;  
    },
    
    update: function(){
        for(var name in Game.creeps){
            var pos = Game.creeps[name].pos;
            var roomdata = Memory.roadStats.current[pos.roomName];
            if(roomdata == null){
                if(manager.getRoomStatus(pos.roomName) == manager.FARM){
                    console.log("Creep " + name + " is in room for which we do not account road stats.");
                    continue;
                }
                this.initRoom(pos.roomName);
                roomdata = Memory.roadStats.current[pos.roomName];
            }
            var stat = roomdata[pos.y * ROOM_SIZE + pos.x];
            
            if(stat.t >= Game.time - 5 &&
               stat.c == name){
                // The same creep was here recently. Do not increment visit counter.
            }else{
                var p = this.getUsageCoeff(pos);
                stat.v += p;
            }
            stat.c = name;
            stat.t = Game.time;
        }
        Memory.roadStats.ticksRecorded++;
    },
    
    statRotate: function(){
        if(Memory.roadStats.ticksRecorded <= ROOM_STAT_ROTATE_PERIOD) return;
        console.log("Rotating room stats.");
    },
    
    run: function() {
        //this.init();
        //this.update();
        //this.statRotate();
    },
    
    getVal: function(rp){
        var roomdata = Memory.roadStats.prev[rp.roomName];
        if(roomdata == null) return null;
        return roomdata[rp.x * ROOM_SIZE + rp.y].visitCount;
    },
    /*
    getBest: function(room) {
        if(Memory.roadStats.ticksRecorded < ROAD_STATS_MIN_TICKS){
            console.log("Not enough stats gathered to find the best position for a new road (" + Memory.roadStats.ticksRecorded + ").");
            return null;
        }
        
        var a = Memory.roadStats.stats.slice();
        
        // Filter out positions where something is already build, or a construction site is in place
        _.remove(a, function(stat){
            var pos = new RoomPosition(stat.x,stat.y,room.name);
            var structs = pos.lookFor(LOOK_STRUCTURES);
            var csites = pos.lookFor(LOOK_CONSTRUCTION_SITES);
            if(structs.length || csites.length) return true;
            return false;
        });
        
        var topPos = _.max(a, function(stat){
            return stat.visitCount;
        });
        
        console.log("Most visited pos has " + topPos.visitCount + " visits");
        if(topPos.visitCount < 10)
            return null;
        
        _.remove(a, function(stat){ return stat.visitCount < topPos.visitCount; });
        
        if(a.length == 0){
            console.log("RS getBest failed to filter correctly, this should never happen.");
            return null;
        }
            
        var res;
        if(a.length == 1)
             res = a[0];
        else{
            console.log("There are " + a.length + " such positions.");
            res = _.sample(a);
        }
        return new RoomPosition(res.x, res.y, room.name);
    }
    */
};

module.exports = roadStatistician;
