var utils = require('utils');
var manager = require('rooms.manager');

var CONTAINER_WAIT_TIME = 1;

var DBG = utils.DBG;
var INF = utils.INF;
var ERR = utils.ERR;
var print = utils.print;


function containerIsEmpty(hubObj){
    if(hubObj.structureType == STRUCTURE_EXTENSION ||
        hubObj.structureType == STRUCTURE_SPAWN ||
        hubObj.structureType == STRUCTURE_TOWER)
        return hubObj.energy == 0;
    else
        return hubObj.store[RESOURCE_ENERGY] == 0;
}

var containers = {

    /* Returns a list of containers which harvest uses, these containers are energy inlets. */
    getContainersGeneratingEnergy: function(){
        return _.filter(Memory.containerData, (c) => c.generatesEnergy);
    },
    /* Returns a list of containers which are close to energy sinks, these containers are energy outlets. */
    getContainersSupplyingEnergy: function(){
        return _.filter(Memory.containerData, (c) => (c.suppliesSpawn || c.suppliesController || c.isTower || c.isStorage));
    },
    /* Returns a list of containers which are storages. */
    getContainerStorages: function(){
        return _.filter(Memory.containerData, (c) => (c.isStorage));
    },

    run: function(){
        // Use this to debug reservations!
        //this.displayReservations();
        if(Game.time % 20 == 7) // TODO: Increase this? Or maybe call rebuildContainerList only when construction is completed?
            this.rebuildContainerList();
    },

    rebuildContainerList: function(){
        print(INF, "Rebuilding containers list.");
        Memory.containerData = [];

        var allRooms = manager.getRooms();
        for(var room in allRooms){
            room = Game.rooms[allRooms[room]];
            if(room == null){
                // Room is not accesible.
                continue;
            }
            var cnts = room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_CONTAINER ||
                                                                  s.structureType == STRUCTURE_TOWER ||
                                                                  s.structureType == STRUCTURE_STORAGE});
            var spawns = room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_SPAWN});
            for(var cnt in cnts){
                cnt = cnts[cnt];
                var id = cnt.id;
                // Is the container used for harvesting?
                var usedForHarvest = false;
                for(var s in Memory.harvest.sources){
                    s = Memory.harvest.sources[s];
                    if(s.storage == id){
                        usedForHarvest = true;
                        break;
                    }
                }
                // Does this container has a spawn nearby?
                var exts = cnt.pos.findInRange(FIND_MY_STRUCTURES, 4, {filter: (s) => s.structureType == STRUCTURE_SPAWN || 
                                                                        s.structureType == STRUCTURE_EXTENSION});
                var hasSpawnNearby = exts.length > 0;
                
                
                // Does this container has a controller nearby?
                var hasControllerNearby = room.controller.pos.getRangeTo(cnt.pos) < 7;
                var roomOwned = manager.getRoomStatus(room.name) == manager.OWN;

                var container_entry = {
                    roomName: room.name,
                    id: id,
                    pos: cnt.pos,
                    generatesEnergy: usedForHarvest,
                    suppliesSpawn: hasSpawnNearby,
                    suppliesController: hasControllerNearby && roomOwned,
                    isStorage: (cnt.structureType == STRUCTURE_STORAGE),
                    isTower: (cnt.structureType == STRUCTURE_TOWER),
                };
                Memory.containerData.push(container_entry);
            }
        }

        Memory.containerData = Memory.containerData.sort((a,b) => a.id < b.id);
    },

    /* Do not use this for grabbing energy, use creepGetEnergy instead. */
    getNearestContainer: function(pos){
        var storage = manager.findNearestInAllRoomsFiltered(FIND_STRUCTURES, (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE),pos);
        return storage;
    },
    /* Do not use this for grabbing energy, use creepGetEnergy instead. */
    getRandomContainer: function(creep, disallow_other_rooms){
        var cts = creep.pos.findInRange(FIND_STRUCTURES, 6, {filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE) && !containerIsEmpty(s)});
        if(cts.length){
            creep.say("nearby");
            console.log("Going nearby instead of trying rand!");
            return _.sample(cts);
        }
        var cts = creep.room.find(FIND_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE});
        return _.sample(cts);
    },

    creepGetEnergy: function(creep, disallow_other_rooms){
        if(disallow_other_rooms == null) disallow_other_rooms = false;
        if(creep.memory.get_energy == null || creep.memory.get_energy.last_tick < Game.time - 3 || creep.memory.get_energy.container == null){
            // First container selected is the nearest one.
            var cnt = this.getNearestContainer(creep.pos);
            if(cnt == null)
                return utils.creepMoveToRandom(creep);
            creep.say("get energy");
            creep.memory.get_energy = {
              start_tick: Game.time,
              container_reach_time: null,
              last_tick: Game.time,
              phase: 0,
              container: cnt.id
            };
        }
        creep.memory.get_energy.last_tick = Game.time;

        var container = Game.getObjectById(creep.memory.get_energy.container);

        if(container == null){
            print(ERR, "Container id " + creep.memory.get_energy.container + " was destroyed!");
            creep.memory.get_energy.container = null;
            return utils.creepMoveToRandom(creep);
        }
        if(container.pos.isNearTo(creep.pos)){
            // Creep is now near the target container.
            if(creep.memory.get_energy.container_reach_time == null){
                // We've just reached it.
                creep.memory.get_energy.container_reach_time = Game.time;
            }else if(creep.memory.get_energy.container_reach_time < Game.time - CONTAINER_WAIT_TIME){
                // Switch to some other container.
                creep.memory.get_energy.container_reach_time = null;
                creep.memory.get_energy.phase++;

                creep.say("try rand");
                container = this.getRandomContainer(creep, disallow_other_rooms);
                if(container == null)
                    return utils.creepMoveToRandom(creep);

                creep.memory.get_energy.container = container.id;
                creep.moveToPOI(container);
                return;
            }
            // Try withdrawing.
            var res = creep.withdraw(container, RESOURCE_ENERGY);
            if(res == OK){
                delete creep.memory.get_energy;
                creep.say("got energy");
                return;
            }
        }else{
            creep.moveToPOI(container);
        }

    },

    displayReservations: function(){
        for(var i in Memory.containerData){
            var cnt = Memory.containerData[i];
            var cntObj = Game.getObjectById(cnt.id);
            if(cntObj == null) continue;
            var type = "";
            if(cnt.generatesEnergy) type += "G";
            if(cnt.suppliesSpawn) type += "S";
            if(cnt.suppliesController) type += "C";
            if(cnt.isTower) type += "T";
            console.log(cnt.id + " " + JSON.stringify(cnt.pos) +  " " + this.getEnergy(cntObj) + " - " + this.getTotalReservationsForContainer(cnt.id) + " = " + this.getEnergyWithReservations(cntObj) + " " + type);
        }
    },

    getTotalReservationsForContainer: function(container_id){
        var total = 0;
        for(var c in Game.creeps){
            var creep = Game.creeps[c];
            if(creep.memory.role == "transporter" && creep.memory.leaf != null && creep.memory.leaf.id == container_id)
                total -= creep.carryCapacity - creep.carry[RESOURCE_ENERGY];
            if(creep.memory.role == "transporter" && creep.memory.hub != null && creep.memory.hub.id == container_id)
                total += creep.carry[RESOURCE_ENERGY];
        }
        return total;
    },

    getEnergy: function(s){
        var total = 0;
        if(s.structureType == STRUCTURE_TOWER)
            total = s.energy - 250;
        else
            total = s.store[RESOURCE_ENERGY];
        return total;
    },

    getEnergyWithReservations: function(s){
        if(s == null) return 0;
        var total = 0;
        if(s.structureType == STRUCTURE_TOWER){
            total = s.energy - 250;
            if(total +  containers.getTotalReservationsForContainer(s.id) > s.energyCapacity){
                return 1000000;
            }
        }else{
            total = s.store[RESOURCE_ENERGY];
            if(total +  containers.getTotalReservationsForContainer(s.id) > s.storeCapacity){
                return 1000000;
            }
        }
        return total + containers.getTotalReservationsForContainer(s.id);
    }

};

module.exports = containers;
