var utils = require('utils');
var manager = require('rooms.manager');

var print = utils.print;
var INF = utils.INF;

var explorersController = {

    name: "Explorers Controller",

    roleName: "explorer",

    prepare: function() {
        if(Memory.roomsToExplore == null){
            Memory.roomsToExplore = [];
        }
        var rooms = manager.getControlledRooms();
        for(var r in rooms){
            var exits = Game.map.describeExits(rooms[r]);
            for(var e in exits){
                if(manager.isUnknown(exits[e]) &&
                   Memory.roomsToExplore.indexOf(exits[e])==-1){
                    Memory.roomsToExplore.push(exits[e]);
                }
            }

        }
        this.desiredSpawns = _.map(Memory.roomsToExplore,(r)=>manager.getClosestRoom(r, manager.OWN));
    },

    getPriorities: function(){
        return _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
    },

    getPriority: function(roomName) {
        if(manager.getCreepsInRoom(roomName,this.roleName).length>0){
            return 0;
        }
        if(this.desiredSpawns.indexOf(roomName)!=-1){
            return 0.9;
        }else{
            return 0;
        }
    },

    getEnergyThreshold: function(){
        return 50;
    },

    getIdealCreep: function(energy, roomName){
        var parts = [MOVE];
        var dst = _.map(Memory.roomsToExplore,(r)=>Game.map.getRoomLinearDistance(roomName, r));
        var minidx = dst.indexOf(_.min(dst));
        var creep = {
            parts: parts,
            name: "E",
            memory: {
                role: this.roleName,
                targetRoom: Memory.roomsToExplore[minidx]
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    run: function(room){
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
    },

    /** @param {Creep} creep **/
    move: function(creep){
        var targetRoom = creep.memory.targetRoom;

        Memory.roomsToExplore = _.filter(Memory.roomsToExplore, (r)=> r != targetRoom);
        if(targetRoom != null){
            var pos = new RoomPosition(25, 25, targetRoom);
            creep.say("GOTO " + targetRoom);
            if(creep.room.name == targetRoom){
                manager.initRoom(creep.room, 1);
                print(INF, "Brave explorer got new room " +  creep.memory.targetRoom+ "!");
            }
            creep.moveTo(pos, {reusePath: 20});
        }else{
            creep.suicide();
        }
    }
};

module.exports = explorersController;
