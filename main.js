var utils = require('utils');
var harvestController = require('harvest.controller');
var transportContoller = require('transport.controller');
var upgradeController = require('upgrade.controller');
var buildController = require('build.controller');
var explorerController = require('explorer.controller');
var claimerController = require('claimer.controller');
var spawnKeeperController = require('spawnkeeper.controller');
var storageKeeperController = require('storagekeeper.controller');
var roadStatistician = require('road.statistician');
var warriorController = require('warrior.controller');
var manager = require('rooms.manager');
var containers = require('containers');
var gameMode = require('gamemode');
var poi = require('poi');
var links = require('links');
var trader = require('trader');
var roads = require('roads');
var profiler = require('profiler');

var DBG = utils.DBG;
var INF = utils.INF;
var ERR = utils.ERR;
var print = utils.print;

var controllers = [harvestController, transportContoller, explorerController, buildController, upgradeController, spawnKeeperController, storageKeeperController, warriorController, claimerController];

function getRowMaximas(arr2d, blocked){
    var n = arr2d[0].length;
    var maxs = [];
    for(var i=0; i<n; i++){
        if(blocked[i]){
            maxs.push(-1);
            continue;
        }
        var row = _.map(arr2d, (x)=>x[i]);
        while(row.length>0){
            var m = row.indexOf(_.max(row));
            if(maxs.indexOf(m)==-1){
                maxs.push(m);
                break;
            }else{
                delete row[m];
            }
        }
    }
    return maxs;
}

var spawn = function(){
    var spawns = Game.spawns;
    var roomNames = manager.getControlledRooms();
    // TODO: Priorities should be computed only for free spawns
    var priorites = _.map(controllers, (x)=>x.getPriorities());
    var busy = _.map(manager.getSpawnAvailable(), (x)=>!x);
    var maximas = getRowMaximas(priorites, busy);
    for(var r = 0; r < roomNames.length; r++){
        var room = Game.rooms[roomNames[r]];
        var spawns = room.find(FIND_STRUCTURES, {filter:(s)=>s.structureType==STRUCTURE_SPAWN});
        if(maximas[r]==-1){
            continue;
        }
        var active =  controllers[maximas[r]];
        print(INF, room + " => " + active.name);
        
        var energy = room.energyAvailable;
        var energyCapacity = room.energyCapacityAvailable;
        var prio = active.getPriority(room.name);
        if(prio <= 0){
            continue;
        }
        if(prio < 1.0){
            var threshold = energyCapacity;
            if(active.getEnergyThreshold != null){
                threshold = Math.min(energyCapacity, active.getEnergyThreshold());
            }
            if(energy < threshold){
                continue;
            }
            print(INF, "Spawns in " + roomNames[r] + " reached threshold energy: " + energy + "/" + energyCapacity);
        }
        var idealCreep = active.getIdealCreep(energy, room.name);
        if(idealCreep != null){
            // Select a non-busy spawn
            for(var s in spawns){
                if(spawns[s].spawning != null) continue; // Spawn is busy.
                var retVal = spawns[s].createCreep(
                    idealCreep.parts,
                    utils.genUniqueName(idealCreep.name),
                    idealCreep.memory
                );
                print(INF, "Spawn result: " + retVal);
                break; // Don't follow to next spawn
            }
        }
    }
    return;
}

module.exports.loop = function () {
    // DO NOT PERFORM MEMORY INITIALIZATION HERE, it is done few lines below, where we check if there are no creeps present.
    PathFinder.use(true);
    profiler.init();

    // Prepare global prototype modifications.
    utils.preparePrototypes();
    poi.preparePrototypes();
    
    /* Cleanup if creep dies */
    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            if(Memory.creeps[name].role === "harvester"){
                var tgt = Memory.creeps[name].target;
                var force = Memory.creeps[name].workForce;
                if(tgt && Memory.harvest.sources){
                    Memory.harvest.sources[tgt].neededForce+=force;
                    Memory.harvest.sources[tgt].harvesters--;
                }
            }
            delete Memory.creeps[name];
        }
    }

    /* If there are no creeps, then apparently we've just respawned. Clear memory. Start on next turn. */
    if(Memory.creeps == null || Object.keys(Memory.creeps).length == 0){
        print(INF, "New start! Clearing memory!!!");
        utils.clearMemory();
        // ---
        utils.initMemory();
        harvestController.initMemory();
        manager.initMemory();
        warriorController.initMemory();
    }
    
    var roomName = null; var room = null;
    profiler.measure("utils", () => {
    
        if(manager.getRooms().length == 0){
            room =  _.values(Game.rooms)[0];
            roomName = room.name;
            print(INF, "No rooms! Initializing first one:" + roomName);
            manager.initRoom(room, 0);
        }else{
            roomName = manager.getControlledRooms()[0];
            room = Game.rooms[roomName];
        }
    
        // Run rooms management
        manager.run();
        /* Run some utils */
        gameMode.run();
        containers.run();
        //poi.run();
        roads.run();
        links.run();
        trader.run();
    });

    profiler.measure("prepare", () => {
        /* Prepare controllers */
        controllers.forEach((c) => c.prepare());
    });

    // Display controller priorites
    var str = gameMode.asString();
    if(Memory.verbosity>4) str += " " + Game.time % 10000 + " ";
    print(DBG, str);
    str = '';
    controllers.forEach((c) => str += c.name.substr(0,4) + "["  + c.creepsCount() + "|" + _.map(c.getPriorities(), (x)=>x.toFixed(2)) + "] ")

    print(INF, str);
    // Display current mode
    /* Temporary disabled
    var mainspawn = utils.getMainSpawn();
    var vis = mainspawn.room.visual;
    vis.text(gameMode.asString(), mainspawn.pos.x + 0.06, mainspawn.pos.y + 0.25 + 0.06, {color: 'black', size: 0.8});
    vis.text(gameMode.asString(), mainspawn.pos.x       , mainspawn.pos.y + 0.25       , {color: 'orange', size: 0.8});
    */
    profiler.measure("spawn", () => {
        // Perform spawning
        spawn();
    });
    
    // Run controllers
    controllers.forEach((c) => {
        profiler.measure("run-" + c.name.substr(0,4), () => {
            c.run(roomName);
        });
    });
    
    profiler.finalize();
};
