var utils = require('utils');
var containers = require('containers');
var links = require('links');
var manager = require('rooms.manager');

var print = utils.print;

var storageKeeper = {

    name: "StorageKeeper Controller",

    roleName: "storagekeeper",
    
    prepare: function() {

    },

    getPriorities: function(){
        return _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
    },

    getPriority: function(roomName) {
        var desiredCreeps = 0;
        if(links.getCentralLinkForRoom(roomName) != null)
            desiredCreeps = 1;
        if(manager.getCreepsInRoom(roomName, this.roleName).length >= desiredCreeps){
            return 0;
        }else{
            return 0.95;
        }
    },

    getEnergyThreshold: function(){
        return 500;
    },

    MAX_LEVEL: 2,
    getIdealCreep: function(energy) {
        var basicSet = [CARRY, CARRY, CARRY, CARRY, CARRY, MOVE];
        var baseName = "StK";
        
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            utils.print(2, "Something went really wrong with cost caluclation!");
        }
        var level = Math.floor(energy/basicCost);
        if(level == 0) return null;
        level = Math.min(level, this.MAX_LEVEL); // Do not create unnecessarily large creeps.

        var parts = [];
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }

        var creep = {
            parts: parts,
            name: "StK"+level,
            memory: {
                role: this.roleName,
                storage: null,
                loading: true
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    run: function(){
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
    },
    
    pickTarget: function(){
        var creeps = this.getCreeps();
        for(var room in Memory.linkData){
            var s = Game.rooms[room].storage;
            if(s == null) continue;
            if(_.filter(creeps, (c) => c.memory.storage == s.id).length > 0) return;
            print("Picking storage at " + s.pos.serialize());
            return s.id;
        }
        print("Redundant StorageKeeper!");
        return null;
    },
    
    move: function(creep){
        if(creep.memory.storage == null)
            creep.memory.storage = this.pickTarget();
        if(creep.memory.storage == null) return;
        
        if(creep.memory.loading && creep.carry.energy >0){
            creep.memory.loading = false;
        }else if(!creep.memory.loading && creep.carry.energy == 0){
            creep.memory.loading = true;
        }
        
        var storage = Game.getObjectById(creep.memory.storage);
        var link = storage.pos.findClosestByRange(FIND_MY_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_LINK});
        if(link == null){
            print("Error: no storage link");
            return;
        }
        var terminal = storage.pos.findClosestByRange(FIND_MY_STRUCTURES, {filter: (s) => s.structureType == STRUCTURE_TERMINAL});
        var overStorageThreshold = storage.store.energy > (100000 + 0);
        var overStorageThresholdB = storage.store.energy > (100000 + 5000);
        
        if(creep.memory.loading){
            var target = link;
            if(link.energy == 0 && overStorageThresholdB){
                target = storage;
            }else if(link.energy == 0){
                // Nowhere to take energy from.
                return;
            }
            if(creep.pos.isNearTo(target.pos)){
                creep.withdraw(target, RESOURCE_ENERGY);
            }else{
                creep.moveToPOI(target);
            }
        }else{
            var target = storage;
            if(overStorageThreshold && terminal && terminal.store.energy < terminal.storeCapacity){
                target = terminal;
            }
            if(creep.pos.isNearTo(target.pos)){
                creep.transfer(target, RESOURCE_ENERGY);
            }else{
                creep.moveToPOI(target);
            }
        }
    }
};

module.exports = storageKeeper;
