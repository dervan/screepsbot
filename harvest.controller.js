var utils = require('utils');
var manager = require('rooms.manager');
var gameMode = require('gamemode');

var DBG = utils.DBG;
var INF = utils.INF;
var VRB = utils.VRB;
var ERR = utils.ERR;
var print = utils.print;

function initializeSource(sourceId, room){
    var d = [-1,0,1];
    var d2 = [-2,-1,0,1,2];
    var workers = [];

    var source = utils.getObject(sourceId);
    var sourcePos = source.pos;
    /* Calculate workers capability */
    for(var x in d){
        for(var y in d){
            if(Game.map.getTerrainAt(sourcePos.x+d[x], sourcePos.y+d[y], room.name)!="wall")
                workers.push([sourcePos.x+d[x], sourcePos.y+d[y]]);
        }
    }

    /* Calculate optimal position for container */
    var minDist = 1000;
    var minPos = null;
    /* If source is processed or it have already a container */
    var build_container = true;
    for(var x in d2){
        for(var y in d2){
            if((Math.abs(d2[x])!=2 && Math.abs(d2[y])!=2) ||
                !build_container){
                continue;
            }
            if(sourcePos.x+d2[x] <= 1 || sourcePos.x+d2[x] >= 48 ||
               sourcePos.y+d2[y] <= 1 || sourcePos.y+d2[y] >= 48){
                continue;
            }

            var newContainerPosition = new RoomPosition(sourcePos.x+d2[x], sourcePos.y+d2[y], room.name);

            if(Game.map.getTerrainAt(newContainerPosition)!="plain" &&
                Game.map.getTerrainAt(newContainerPosition)!="swamp"){
                continue;
            }

            /* Check for existence of containers or potential containers */
            var constructionSiteHere = room.lookForAt(LOOK_CONSTRUCTION_SITES, newContainerPosition);
            var containerHere = room.lookForAt(LOOK_STRUCTURES, newContainerPosition);
            constructionSiteHere = _.filter(constructionSiteHere, (s) => s.structureType == STRUCTURE_CONTAINER);
            containerHere = _.filter(containerHere, (s) => s.structureType == STRUCTURE_CONTAINER);
            if(constructionSiteHere.length > 0 ||
               containerHere.length > 0){
                build_container = false;
                break;
            }

            /* Calculate score for this place */
            //var dist = newContainerPosition.findPathTo(utils.getRoomSpawn(room.name)).length;
            //dist += utils.accessPenalty(newContainerPosition)*2;
            var dist = 0;
            for(var v in workers){
                dist += newContainerPosition.findPathTo(workers[v][0], workers[v][1]).length;
            }
            if(minDist>dist){
                minDist = dist;
                minPos = newContainerPosition;
            }
        }
    }
    if(build_container){
        var ret = minPos.createConstructionSite(STRUCTURE_CONTAINER);
        var constructionSite = minPos;
        if(ret === 0){
            print(VRB, "New container on " + minPos + " cumulative distance " + minDist);
        }
    }else{
        if(constructionSiteHere.length > 0){
            print(DBG, "Recycled construction site");
            constructionSite = constructionSiteHere[0].pos;
        }
        if(containerHere.length > 0){
            print(DBG, "Recycled container");
            var sourceStorage = containerHere[0].id;
        }

    }

    print(VRB, "Adding source " + sourceId + room.name + sourcePos.x + sourcePos.y);
    var neededForce = source.energyCapacity / 300 + 2;
    print(VRB, "It needs " + neededForce + " force.");

    // Count harvesters that are already bound to this source, and get their total force
    var harv_bound = [];
    var total_force = 0;
    for(var creep in Game.creeps){
        creep = Game.creeps[creep];
        if(creep.memory.role == null || creep.memory.role != "harvester") continue;
        if(creep.memory.target == null || creep.memory.target != sourceId) continue;
        harv_bound.push(creep.name);
        total_force += creep.memory.workForce;
    }
    print(VRB, "Harvesters already bound to this source: " + harv_bound.length + ", their force: " + total_force);

    var ret = {
        source: sourceId,
        harvesters: harv_bound.length,
        harvesters_creeps: harv_bound,
        harvesters_limit: workers.length,
        neededForce: neededForce - total_force,
        room_pos: new RoomPosition(sourcePos.x, sourcePos.y, room.name),
        storage: sourceStorage,
        constructionSite: constructionSite
    };
    Memory.harvest.sources[sourceId] = ret;
}

function fixStorage(sourceId){
    var sourcePos = utils.getObject(sourceId).pos;
    var room = utils.getObject(sourceId).room;
    var storages = room.find(FIND_STRUCTURES, {filter: couldStoreEnergy});
    var storage = sourcePos.findClosestByRange(storages);
    if(storage == null)
        storage = utils.getRoomSpawn(room.name);
    Memory.harvest.sources[sourceId].storage = storage.id;
    return storage;
}

function attachCreep(creep, sourceId){
        if(sourceId == null){
            utils.creepMoveToRandom(creep);
            return;
        }
        creep.memory.target = sourceId;
        if(Memory.harvest.sources[sourceId] != null){
            Memory.harvest.sources[sourceId].harvesters++;
            Memory.harvest.sources[sourceId].neededForce -= creep.memory.workForce;
        }else{
            print(ERR,"Wrong target for creep " + creep.name + "!");
            return;
        }
}


function getBestSource(){
    return _.max(_.filter(Memory.harvest.sources, (s) => s.harvesters < s.harvesters_limit), (s) => s.neededForce).source;
}

function couldStoreEnergy(s){
    return ((s.structureType == STRUCTURE_EXTENSION ||
             s.structureType == STRUCTURE_SPAWN ||
             s.structureType == STRUCTURE_TOWER) &&
            s.energy < s.energyCapacity) ||
            (s.structureType == STRUCTURE_CONTAINER &&
             s.store[RESOURCE_ENERGY] < s.storeCapacity);

}

var harvestersController = {

    name: "Harvesters Controller",

    roleName: "harvester",

    prepare: function() {
        var needed = 0;
        var total = 0;
        var totalNeededForce = 0;
        var special = false;
        var count = this.creepsCount();

        if(Memory.harvest == null){
            Memory.harvest = {};
        }
        if(Memory.harvest.rooms == null){
            Memory.harvest.rooms = [];
        }
        var rooms = manager.getRooms();
        for(var r in rooms){
            if(_.find(Memory.harvest.rooms, (x)=>x==rooms[r]) == undefined &&
            Game.rooms[rooms[r]] != null){
                this.initRoom(Game.rooms[rooms[r]]);
            }
        }
        for(var s in Memory.harvest.sources){
            var src = Memory.harvest.sources[s];
            if(src.harvesters_limit != null){
                total  += src.harvesters_limit;
            }
            if(src.harvesters < src.harvesters_limit && src.neededForce != null){
                totalNeededForce += Math.max(src.neededForce, 0);
            }
            if(src.harvesters == 0){
                special = true;
            }
        }
        print(DBG, "Harvester prio: neededForce: " +totalNeededForce + " total " + count + "/" + total + " |special " + special);
        let best_source = getBestSource();
        if(!best_source){
            this.priority = 0;
            return this.priority;
        }
        var nextSpawn = manager.findNearestInAllRoomsFiltered(
            FIND_STRUCTURES,
            (x)=>x.structureType==STRUCTURE_SPAWN,
            RoomPosition.fromObj(Memory.harvest.sources[best_source].room_pos));
        this.nextRoom = nextSpawn.room.name;
        if(_.some(manager.getControlledRooms(), (x)=>gameMode.isBootstrap(x))){
            if(count > 0 && (count == total || totalNeededForce == 0)){
                this.priority = 0;
            }else{
                this.priority = 2;
            }
        }else
            /*if(gameMode.isUpgradeOnly()){
            // Do not prioritize harvesters in upgrade only mode.
            if(count > 0 && (count == total || totalNeededForce == 0)){
                this.priority = 0;
            }else{
                this.priority = 0.15;
            }*/
        {
            if(special){
                this.priority = 0.999;
            }else{

                if(total == 0 || totalNeededForce == 0)
                    this.priority = 0;
                else
                    this.priority = Math.pow(1-Math.min(count, total)/total, 0.15);
            }
        }
        return this.priority;
    },

    getPriorities: function(){
        var priorites = _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
        var maxp = _.max(priorites);
        if(maxp==0)
            return _.map(priorites, (x)=>0);
        else
            return _.map(priorites, (x)=>(x/maxp)*this.priority);
    },

    getPriority: function(roomName){
        if(this.priority == null){
            print(ERR, "Priority not calculated in " + this.name);
            return -0.01;
        }else{
            if(this.nextRoom == null)
                return 0;
            if(roomName==null){
                roomName = this.nextRoom;
            }
            var weight = Math.min(1.0/(0.15+Math.pow(Game.map.getRoomLinearDistance(roomName, this.nextRoom), 1/4)), 1);
            if(isNaN(weight))
                weight = 1;
            return this.priority*weight;
        }
    },

    getEnergyThreshold: function(){
        return 300;
    },

    MAX_LEVEL: 3,
    getIdealCreep: function(energy, roomName) {
        if(gameMode.isBootstrap(roomName)){
            var basicSet = [WORK, CARRY, MOVE];
        }else{
            var basicSet = [WORK, WORK, CARRY, MOVE];
        }
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            print(ERR, "Something went really wrong with cost caluclation!");
        }

        var level = Math.floor(energy/basicCost);
        if(level == 0) return null;
        level = Math.min(level, this.MAX_LEVEL); // Do not create unnecessarily large creeps.

        var parts = [];
        var bonus = 0;
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }
        if(level<3){
            var rest = energy - utils.calculateSpawnCost(parts);
            bonus = Math.floor(rest/100);
            for(var i=0; i<bonus;i++){
                parts = parts.concat([WORK]);
            }
        }
        var workParts = _.filter(parts, (part) => part == WORK).length;
        var creep = {
            parts: parts,
            name: "H"+level+"|"+bonus,
            memory: {
                role: this.roleName,
                target: null,
                workForce: 2*workParts
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    clearMemory: function(){
        delete Memory.harvest;
        Memory.harvest = {};
        Memory.harvest.sources = {};
        Memory.harvest.rooms = [];
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            creeps[i].memory.source = undefined;
        }
    },

    initMemory: function(){
        if(Memory.harvest == null)
            Memory.harvest = {};
        if(Memory.harvest.sources == null)
            Memory.harvest.sources = {};
        if(Memory.harvest.rooms == null)
            Memory.harvest.rooms = [];
    },

    run: function(roomName){
        if(_.find(Memory.harvest.rooms, (x)=>x == roomName) == undefined){
            print(ERR, "Harvest launched in unknown room " + roomName + ". Initializating!");
            this.initRoom(Game.rooms[roomName]);
        }
        if((Game.time & 63) == 7 || Memory.harvest.requestClean == 1){
            print(INF, 'Clearing harvest memory!');
            Memory.harvest.rooms = [];
            var rooms = manager.getRooms();
            var sources = Memory.harvest.sources;
            // This initializes sources we used to see but can no longer reach due to fog of war.
            for(var s in sources){
                // Assume 1500 energy
                sources[s].neededForce = 7;

                // Count harvesters that are already bound to this source, and get their total force
                var harv_bound = [];
                var total_force = 0;
                for(var creep in Game.creeps){
                    creep = Game.creeps[creep];
                    if(creep.memory.role == null || creep.memory.role != "harvester") continue;
                        if(creep.memory.target == null || creep.memory.target != s) continue;
                    harv_bound.push(creep.name);
                    total_force += creep.memory.workForce;
                }
                print(VRB, "Harvesters already bound to this source: " + harv_bound.length + ", their force: " + total_force);

                sources[s].harvesters = harv_bound.length;
                sources[s].harvesters_creeps = harv_bound;
                sources[s].neededForce -= total_force;
            }
            // This initializes sources we CAN see.
            for(var r in rooms){
                print(VRB, 'Initializing ' + r +" " + rooms[r]);
                this.initRoom(Game.rooms[rooms[r]]);
            }
            Memory.harvest.requestClean = 0;
            print(DBG, _.values(Memory.harvest.sources).length + " sources | " +
                Memory.harvest.rooms.length + " rooms | " +
                this.creepsCount() + " harvesters");
        }
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            //if(creeps[i].spawning) continue;
            this.move(creeps[i]);
        }
    },


    /** @param {Creep} creep **/
    move: function(creep) {
        if(_.find(Memory.harvest.rooms, (x)=>(x==creep.room.name)) == undefined){
            print(INF, "Harvester in unknown room " + creep.room.name);
            //this.initRoom(Game.rooms[creep.room.name]);
            return;
        }
        /* If the creep is using an invalid source, remove the binding */
        if(Memory.harvest.sources[creep.memory.target] == null)
            creep.memory.target = null;

        /* Binding creep to source */
        if(creep.memory.target == null){
            var sourceId = getBestSource();
            print(VRB, "Harvester " + creep.memory.name + " attached to " + sourceId);
            attachCreep(creep, sourceId);
        }

        if(creep.carry.energy < creep.carryCapacity){
            /* Harvesting */

            /* Try to get source object. If it's in currently invisible room, try to use room
             * reference */
            var source = utils.getObject(creep.memory.target);
            if(source == null){
                var target_id = creep.memory.target;
                if(Memory.harvest.sources[target_id] == null){
                    creep.memory.target = undefined;
                    print(ERR, "Erroneous source id " + target_id + ". Forget it!");
                    return;
                }
                if(Memory.harvest.sources[target_id].room_pos == null){
                    print(ERR, "Erroneous source description for id " + target_id + ". Forget it!");
                    delete Memory.harvest.sources[target_id];
                    creep.memory.target = null;
                }else{
                    var room_pos = Memory.harvest.sources[target_id].room_pos;
                    var rp = new RoomPosition(room_pos.x, room_pos.y, room_pos.roomName);
                    creep.moveToPOI(rp, {reusePath: 40});
                }

            }
            var res = creep.harvest(source);
            if(res == ERR_NOT_IN_RANGE){
                res = creep.moveToPOI(source);
                if(Game.time & 15 == 6){
                    creep.say(source.id.slice(-5));
                }
            }
        }
        else
        {
            var source = Memory.harvest.sources[creep.memory.target];

            if(gameMode.isBootstrap(creep.room.name)){
                /* Transfer energy to spawn */
                var spawn = utils.getRoomSpawn(creep.room.name);
                if(spawn != null ){
                    if(creep.transfer(spawn, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                        creep.moveToPOI(spawn);
                    }
                    return;
                }
            /* Creep is tied to construction site */
            }
            if(source.constructionSite != null){
                /* Building phase */
                if(source.constructionSite.roomName != creep.room.name){
                    var pos = new RoomPosition( source.constructionSite.x, source.constructionSite.y, source.constructionSite.roomName);
                    creep.moveToPOI(pos);
                    return;
                }
                var site = new RoomPosition(source.constructionSite.x, source.constructionSite.y, source.constructionSite.roomName);
                var target = site.lookFor(LOOK_CONSTRUCTION_SITES)[0];
                if(site != null && target == null){
                    print(DBG, "Building completed? " + creep.memory.target);
                    Memory.harvest.sources[creep.memory.target].constructionSite = undefined;
                    return;
                }
                var res = creep.build(target);
                if(res == ERR_NOT_IN_RANGE){
                    creep.moveToPOI(target);
                }else if(res != OK){
                    print(ERR, "Cannot build container - error " + res);
                    creep.say("Err"+res);
                }
            }else{
                var storage = Memory.harvest.sources[creep.memory.target].storage;
                if(storage == null){
                    storage = fixStorage(creep.memory.target);
                    print(ERR, "Null storage in source " + creep.memory.target);
                    return;
                }
                var storageObj = utils.getObject(storage);
                if(storageObj == null){
                    print(ERR, "Cannot get storage object in source " + creep.memory.target);
                    return;
                }
                if(storageObj.structureType == STRUCTURE_CONTAINER && storageObj.hits<100000){
                    if(creep.repair(storageObj) == ERR_NOT_IN_RANGE){
                        creep.moveToPOI(storageObj);
                    }
                }else{
                    // Transfer energy to container
                    var res = creep.transfer(storageObj, RESOURCE_ENERGY);
                    if(couldStoreEnergy(storageObj) && res == ERR_NOT_IN_RANGE){
                        creep.moveToPOI(storageObj);
                    }else if(res == ERR_FULL){
                        creep.repair(storageObj);
                    }
                }
            }
        }

        // Finally, display a visual link between harvester and target source
        var linePos = Memory.harvest.sources[creep.memory.target].room_pos;
        utils.multiroomLine(creep.pos, linePos, {width: 0.2, color: '#ffe56d'});
        utils.multiroomLine(creep.pos, linePos, {width: 0.05, color: '#ffff6d'});
    },

    initRoom: function(room) {
        if(room == null){
            print(ERR, "Trying to initialize null room!");
            return;
        }
        if(_.find(Memory.harvest.rooms, (x)=>(x==room.name)) != undefined){
            print(INF, "Room " + room.name + " is already known for harvester! ");
            return;
        }
        if(manager.getRoomStatus(room.name)==manager.BLACKLIST){
            print(DBG, "Room " + room.name + " is on blacklist");
            return;
        }

        print(VRB, "Init room" + room + room.name);
        Memory.harvest.rooms.push(room.name);
        if(manager.isUnknown(room.name)){
            manager.initRoom(room, manager.FARM);
        }
        var sources = room.find(FIND_SOURCES);
        for(var s in sources){
            initializeSource(sources[s].id, room);
        }
    }
};

module.exports = harvestersController;
