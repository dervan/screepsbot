var utils = require('utils');
var manager = require('rooms.manager');
var warrior = require('warrior.controller');

var MODE_BOOTSTRAP = "bootstrap";
var MODE_NORMAL = "normal";
var MODE_UPGRADE_ONLY = "upgradeOnly";
var MODE_DEFENSE = "defense";


/* Game mode state automata. */
var gameMode = {
    switchMode: function(roomName, newmode){
        var oldmode = Memory.gameModes[roomName];
        if(oldmode == null) oldmode = "NONE";
        utils.print("Switching game mode from " + oldmode + " to " + newmode);
        Memory.gameModes[roomName] = newmode;
    },
    
    initGameMode: function(){
        if(Memory.gameModes == null){
            if(Memory.gameMode != null){
                var newMode = Memory.gameMode;
            }else{
                var newMode = MODE_BOOTSTRAP;
            }
            Memory.gameModes = {}
            var rooms = manager.getControlledRooms();
            for(var r in rooms){
                r = rooms[r];
                Memory.gameModes[r] = newMode;
            }
        }
        // Remove old game state data, to avoid confusion.
        delete Memory.gameMode;
    },
    
    updateGameMode: function(roomName){
        // Please keep this switch table organized.

        if(Memory.gameModes[roomName] == null){
            Memory.gameModes[roomName] = MODE_BOOTSTRAP;
        }else if(Memory.gameModes[roomName] == MODE_BOOTSTRAP){
            if(this.getBootstrapDone(roomName))
                this.switchMode(roomName, MODE_NORMAL);
        }else if(Memory.gameModes[roomName] == MODE_NORMAL){
            //this is TODO
            //if(warrior.underAttack())
            //{
            //    this.switchMode(MODE_DEFENSE);
            //}

            // TODO: Separate game modes per each owned room?
            //var main_storage = utils.getMainController().room.storage;
            //if(main_storage != undefined && main_storage.store[RESOURCE_ENERGY] > 950000)
            //    this.switchMode(MODE_UPGRADE_ONLY);
        }else if(Memory.gameModes[roomName] == MODE_UPGRADE_ONLY){
            //var main_storage = utils.getMainController().room.storage;
            //if(main_storage.store[RESOURCE_ENERGY] <  890000)
            //    this.switchMode(MODE_NORMAL);
        }
    },
    
    run: function(){
        this.initGameMode();
        var rooms = manager.getControlledRooms();
        for(var r in rooms){
            this.updateGameMode(rooms[r]);
        }

    },
    
    isBootstrap:  function(roomName){ return Memory.gameModes[roomName] == MODE_BOOTSTRAP   ;},
    isNormal:     function(roomName){ return Memory.gameModes[roomName] == MODE_NORMAL      ;},
    isUpgradeOnly:function(roomName){ return Memory.gameModes[roomName] == MODE_UPGRADE_ONLY;},
    
    asString: function(){
        return "[" + JSON.stringify(Memory.gameModes)+ "]";
    },
    
    // This SHOULD be implemented in harvest, but we need to somehow break circular dependency between harvest and this module.
    getBootstrapDone: function(roomName){
        var count = 0;
        var total = 0;
        var totalNeededForce = 0;
        var creeps = manager.getCreepsInRoom(roomName, "harvester");
        var count = creeps.length;
        if(Memory.harvest == null){
            return 0;
        }
        for(var s in Memory.harvest.sources){
            // Ignore sources in rooms we don't control.
            var source = Game.getObjectById(Memory.harvest.sources[s].source);
            if(source == null || manager.getRoomStatus(source.room.name) > 0 || source.room.name != roomName) continue;
            if(Memory.harvest.sources[s].harvesters_limit != null){
                total  += Memory.harvest.sources[s].harvesters_limit;
            }
            if(Memory.harvest.sources[s].neededForce != null){
                totalNeededForce += Math.max(Memory.harvest.sources[s].neededForce, 0);
            }
        }
        // Bootstrap is done when we've spawned all harvesters possible.
        return (count > 0 && (count >= total || totalNeededForce <= 0));
    }

};

module.exports = gameMode;
