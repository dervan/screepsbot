var ROOM_SIZE = 50;

var DBG = 5;
var VRB = 4;
var INF = 3;
var ERR = 2;
var VIP = 1;
var prefixes = ['[!!!] ', '[!!!] ', '[ERR] ', '[INF] ', '[VRB] ', '[DBG] '];

var utils = {
    /* Provides various utility functions that may be useful anywhere else. */
    DBG: DBG, 
    VRB: VRB, 
    INF: INF,
    ERR: ERR,
    VIP: VIP,

    preparePrototypes: function(){
        var utils = this;
        RoomPosition.prototype.multiroomRangeTo = function(pos2){
            pos2 = utils.correctRoomPosForLine(this.roomName, pos2);
            return this.getRangeTo(pos2);
        };
        RoomPosition.fromObj = function(pos){
            return new RoomPosition(pos.x, pos.y, pos.roomName)  ;
        };
        RoomPosition.prototype.isEnterable = function () {
            var atPos = this.look();
            var SWAMP = "swamp";
            var PLAIN = "plain";
            for ( var i = 0 ; i < atPos.length ; i++ )
            {
                switch (atPos[i].type) {
                    case LOOK_TERRAIN:
                        if (atPos[i].terrain != PLAIN && atPos[i].terrain != SWAMP)
                            return false;
                        break;
                    case LOOK_STRUCTURES:
                        if (OBSTACLE_OBJECT_TYPES.includes(atPos[i].structure.structureType))
                            return false;
                        break;
                    case LOOK_CREEPS:
                    case LOOK_SOURCES:
                    case LOOK_MINERALS:
                    case LOOK_NUKES:
                    case LOOK_ENERGY:
                    case LOOK_RESOURCES:
                    case LOOK_FLAGS:
                    case LOOK_CONSTRUCTION_SITES:
                    default:
                }
            }
            return true;
        };
        RoomPosition.prototype.isOnEdge = function () {
            return this.x == 0 || this.x == 49 || this.y == 0 || this.y == 49;
        }
    },

    initMemory: function(){
        if(Memory.utilsInited) return;
        Memory.arrangeIndex = 0;
        Memory.uNameID = 0;
        Memory.utilsInited = true;
        Memory.verbosity = 4;
    },

    getObject: function(id){
        return Game.getObjectById(id);
    },
    
    print: function(level, logString){
        if(typeof level != "number" || logString == null){
            logString = level;
            level = INF;
        }
        if(level<=Memory.verbosity)
            console.log(prefixes[level] + logString);
    },

    getRoomSpawn: function(roomName){
        var spawn = _.filter(_.values(Game.spawns), (x)=>x.room.name==roomName)[0];
        if(spawn === null)
            this.print(VIP, " There is no spawns in game");
        return spawn;
    },
    
    getMainLevel: function(){
        var f = _.filter(Memory.roomsData, (x)=>x.status==0);
        return  _.max(_.map(f, (x)=>this.getControllerLevel(Game.rooms[x.name].controller)));
    },

    getRoomLevel: function(roomName){
        if(Game.rooms[roomName]==null || Game.rooms[roomName].controller == null){
            return 0;
        }
        return this.getControllerLevel(Game.rooms[roomName].controller);
    },
    
    getControllerLevel: function(controller){
        if(controller == null || controller.my == false){
            return 0;
        }
        if(controller.level == 8 || controller.level == 0){
            return controller.level;
        }else{
            return controller.level + controller.progress/controller.progressTotal;
        }
    },
    
    genUniqueName: function(name){
        return "(" + (Memory.uNameID++).toString() + ") " + name;
        if(Memory.uNameID >= 10000){
            Memory.uNameID = 0;
        }
    },
    
    clearMemory: function(){
        delete Memory.harvest;
        delete Memory.warrior;
        delete Memory.uNameID;
        delete Memory.arrangeIndex;
        delete Memory.utilsInited;
        Memory.creeps = {};
        delete Memory.gameMode;
        delete Memory.roadStats;
        delete Memory.roomStats;
    },
    
    creepMoveToRandom: function(creep){
        // Nothing to do. To avoid blocking access to Source, go to a random position (not outside room!).
        if(creep.memory.park_target === null || creep.memory.park_target === undefined){
            var pt = null
            while(pt == null){
                var X = Math.floor(Math.random() * (ROOM_SIZE - 2) +  1);
                var Y = Math.floor(Math.random() * (ROOM_SIZE - 2) +  1);
                // TODO: Check if position is empty, if not, sample another one.
                var pt = new RoomPosition(X,Y,creep.room.name);
                if(Game.map.getTerrainAt(pt) == 'plain' && pt.lookFor(LOOK_STRUCTURES).length == 0){
                    creep.memory.park_target = pt;
                    creep.say("park " + pt.x + " " + pt.y);
                }else{
                    pt = null;
                }
            }
        }
        var pt = creep.memory.park_target;
        creep.moveTo(pt.x, pt.y);
        creep.room.visual.text("?", creep.pos, {color: 'blue'});
    },

    calculateSpawnCost: function(parts) {
        if(parts == null)
            return 0;
        var total = 0;
        parts.forEach( function(part){
            if(part == MOVE) total += 50;
            else if(part == WORK) total += 100;
            else if(part == CARRY) total += 50;
            else if(part == ATTACK) total += 80;
            else if(part == TOUGH) total += 10;
            else if(part == RANGED_ATTACK) total += 150;
            else if(part == HEAL) total += 250;
            else if(part == CLAIM) total += 600;
            else this.print(ERR, "calculateSpawnCost: FIXME!!!");
        });
        return total;
    },

    diffToDirection: function(x, y) {
        if(x == -1 && y == -1)
            return TOP_LEFT;
        else if(x == -1 && y == 0)
                return LEFT;
        else if(x == -1 && y == 1)
                return BOTTOM_LEFT;
        else if(x == 0 && y == -1)
                return TOP;
        else if(x == 0 && y == 0)
                return null;
        else if(x == 0 && y == 1)
                return BOTTOM;
        else if(x == 1 && y == -1)
                return TOP_RIGHT;
        else if(x == 1 && y == 0)
                return RIGHT;
        else if(x == 1 && y == 1)
                return BOTTOM_RIGHT;
    },

    accessPenalty: function(pos){
        var room = Game.rooms[pos.roomName];
        var ngh = room.lookAtArea(pos.y-1,pos.x-1,pos.y+1,pos.x+1, true);
        var result = 0;
        for(var n in ngh){
            if(ngh[n].terrain == 'wall'){
                result++;
            }
        }
        return result;
    },

    buildAccompanyingContainer: function(pos, name){
        var room = Game.rooms[pos.roomName];
        var d2 = [-3,-2,-1,0,1,2,3];
        d2 = _.shuffle(d2);
        var build_container = true;
        var minDist = 1000;
        var minPos = pos;
        
        for(var x in d2){
            for(var y in d2){
                if((Math.abs(d2[x])<2 && Math.abs(d2[y])<2) ||
                    !build_container){
                    continue;
                }

                var newContainerPosition = new RoomPosition(pos.x+d2[x], pos.y+d2[y], room.name);

                if(Game.map.getTerrainAt(newContainerPosition)!="plain" &&
                    Game.map.getTerrainAt(newContainerPosition)!="swamp")
                    continue;

                /* Check for existence of containers or potential containers */
                var constructionSiteHere = room.lookForAt(LOOK_CONSTRUCTION_SITES, newContainerPosition);
                var containerHere = room.lookForAt(LOOK_STRUCTURES, newContainerPosition);
                if(constructionSiteHere.length > 0 ||
                   containerHere.length > 0){
                    build_container = false;
                    break;
                }

                /* Calculate score for this place */
                var dist = this.accessPenalty(newContainerPosition);
                if(minDist>dist){
                    minDist = dist;
                    minPos = newContainerPosition;
                }
            }
        }
        if(build_container){
            var ret = minPos.createConstructionSite(STRUCTURE_CONTAINER);
            var constructionSite = minPos;
            if(ret === 0){
                this.print(INF, "New container on " + name);
            }
        }
        
    },

    genParts: function(n, part) {
        var res = []
        for(var i = 0; i < n; i++)
            res.push(part)
        return res
    },
    
    parseRoomName: function(roomName) {
        var E = roomName.indexOf("E");
        var W = roomName.indexOf("W");
        var S = roomName.indexOf("S");
        var N = roomName.indexOf("N");
        var _1 = Math.max(E,W);
        var _2 = Math.max(S,N);
        var X = parseInt(roomName.substring(_1 + 1, _2));
        var Y = parseInt(roomName.substring(_2 + 1, roomName.length));
        if(W > -1) X *= -1;
        if(S > -1) Y *= -1;
        return {X: X, Y: Y};
    },
    
    correctRoomPosForLine: function(roomName, pos) {
        if(roomName == pos.roomName)
            return pos;
        var thisRoom = this.parseRoomName(roomName);
        var targetRoom = this.parseRoomName(pos.roomName);
        var Xdiff = targetRoom.X - thisRoom.X;
        var Ydiff = targetRoom.Y - thisRoom.Y;
        var newx = pos.x + 50 * Xdiff;
        var newy = pos.y - 50 * Ydiff;
        return new RoomPosition(newx, newy, roomName);
    },
    
    multiroomRangeTo: function(pos1, pos2){
        pos2 = this.correctRoomPosForLine(pos1.roomName, pos2);
        return pos1.getRangeTo(pos2);
    },
    
    multiroomLine: function(from, to, opts) {
        var Froom = Game.rooms[from.roomName];
        var Troom = Game.rooms[to.roomName];
        
        var Ffrom = this.correctRoomPosForLine(from.roomName, from);
        var Fto = this.correctRoomPosForLine(from.roomName, to);
        if(Froom != null)
            Froom.visual.line(Ffrom, Fto, opts);
        
        var Tfrom = this.correctRoomPosForLine(to.roomName, from);
        var Tto = this.correctRoomPosForLine(to.roomName, to);
        if(Troom != null)
            Troom.visual.line(Tfrom, Tto, opts);
    }
};

module.exports = utils;
