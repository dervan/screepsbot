var utils = require('utils');
var manager = require('rooms.manager');

var print = utils.print;
var INF = utils.INF;
var ERR = utils.ERR;
var VRB = utils.VRB;
var DBG = utils.DBG;
var VIP = utils.VIP;
var MAX_DESIRED_RESERVATION = 3500;

var claimerController = {

    name: "Claimer Controller",

    roleName: "claimer",

    prepare: function() {
        if(Memory.roomsToReserve == null){
            Memory.roomsToReserve = [];
        }
        if(Memory.roomsToClaim == null){
            Memory.roomsToClaim = [];
        }
        /* Usually we don't need anything */
        this.desiredSpawns = [];

        if(Memory.roomsToClaim.length > 0){
            /* Got claim request */
            this.mode = 'claim';
            this.nextRoom = Memory.roomsToClaim[0];
            this.priority = 0.85;
            this.desiredSpawns = _.map(Memory.roomsToClaim, (r)=>manager.getClosestRoom(r, manager.OWN));
        }else{
            this.mode = 'reserve';
            var reservedRooms = manager.getReservedRooms();
            var roomsToServe = _.filter(reservedRooms, (x)=>((Game.rooms[x]!=null) && (Game.rooms[x].controller.reservation==null || (Game.rooms[x].controller.reservation.ticksToEnd<MAX_DESIRED_RESERVATION))));
            var needWork = roomsToServe.length + Memory.roomsToReserve.length;
            var totalCount = reservedRooms.length + Memory.roomsToReserve.length;
            print(DBG, "Reservers:" + this.creepsCount() + " RoomsToServe: " +  needWork+ " / " + totalCount);
            var roomsWithoutCreep =[].concat(roomsToServe);
            var creeps = this.getCreeps();
            for(var i=0; i<creeps.length; i++){
                roomsWithoutCreep = _.filter(roomsWithoutCreep, (x)=> x != creeps[i].memory.targetRoom);
            }
            this.nextRoom = null;
            this.roomsNeedsReserver = roomsWithoutCreep.concat(Memory.roomsToReserve);
            this.desiredSpawns = _.map(this.roomsNeedsReserver, (r)=>manager.getClosestRoom(r, manager.OWN));
            print(DBG, "Claimer rooms to be served: " + this.roomsNeedsReserver);
        }
    },

    getPriorities: function(){
        return _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
    },

    getPriority: function(roomName) {
        if(this.desiredSpawns.indexOf(roomName)!=-1 && Game.rooms[roomName].energyCapacityAvailable>1300){
            return 0.9;
        }else{
            return 0;
        }
    },

    getEnergyThreshold: function(){
        return 1300;
    },

    getIdealCreep: function(energy, roomName) {
        if(this.mode != 'claim'){
            var parts = [CLAIM, CLAIM, MOVE, MOVE];
        }else{
            var parts = [CLAIM, MOVE];
        }

        if(this.mode == 'reserve'){
            var dst = _.map(Memory.roomsToReserve, (r)=>Game.map.getRoomLinearDistance(roomName, r));
            var minidx = dst.indexOf(_.min(dst));
            var targetRoom = Memory.roomsToReserve[minidx];
        }else{
            var dst = _.map(Memory.roomsToClaim, (r)=>Game.map.getRoomLinearDistance(roomName, r));
            var minidx = dst.indexOf(_.min(dst));
            var targetRoom = Memory.roomsToClaim[minidx];
        }

        var creep = {
            parts: parts,
            name: "C",
            memory: {
                role: this.roleName,
                mode: this.mode,
                targetRoom: targetRoom
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => (creep.memory.role == this.roleName));
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    run: function(room){
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
    },

    move: function(creep){
        var mode = creep.memory.mode;
        if(mode=='claim'){
            if(creep.memory.targetRoom == null){
                print(VIP, "Claimer not assigned!");
                creep.memory.targetRoom = Memory.roomsToClaim[0];
            }
            var targetRoomLevel = 3;
            Memory.roomsToClaim = _.without(Memory.roomsToClaim, creep.memory.targetRoom);
        }else{
            if(creep.memory.targetRoom == null){
                print(VIP, "Reserver not assigned!");
                creep.memory.targetRoom = this.roomsNeedsReserver[0];
            }
            var targetRoomLevel = 2;
            Memory.roomsToReserve = _.without(Memory.roomsToReserve, creep.memory.targetRoom);
        }

        if(creep.memory.targetRoom != null &&
           manager.getRoomStatus(creep.memory.targetRoom) != targetRoomLevel){
            manager.setStatus(creep.memory.targetRoom, targetRoomLevel);
            print(INF, "Room " + creep.memory.targetRoom + " is initialized");
        }

        /* Go to target room */
        if(creep.memory.targetRoom != creep.room.name){
            var pos = new RoomPosition(25,25, creep.memory.targetRoom);
            creep.say(mode.substr(0,3) + " " + creep.memory.targetRoom);
            creep.moveTo(pos, {reusePath: 20});
            return;
        }else{
            var ctrl = creep.room.find(FIND_STRUCTURES, {filter: (s)=>s.structureType == STRUCTURE_CONTROLLER})[0];
            if(mode == 'claim'){
                /* Go and claim new room! */
                if(ctrl.my == true){
                    print(VIP, "Room " + creep.memory.targetRoom + "already owned!");
                    manager.setStatus(creep.room.name, manager.OWN)
                    manager.initRoom(creep.room, manager.OWN)
                }
                var ret = creep.claimController(ctrl);
                if(ret == ERR_NOT_IN_RANGE){
                    creep.moveTo(ctrl);
                }else if(ret == ERR_GCL_NOT_ENOUGH){
                    creep.memory.mode = 'reserve';
                }else if(ret != OK){
                    print(ERR, "Error " + ret + " when trying to claim room " + creep.memory.targetRoom);
                }
            }else{
                /* Reserve new room */
                var ret = creep.reserveController(ctrl);
                if(ret == ERR_NOT_IN_RANGE){
                    creep.moveTo(ctrl);
                }
            }
        }
    }
};

module.exports = claimerController;
