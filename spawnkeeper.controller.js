var utils = require('utils');
var containers = require('containers');
var gameMode = require('gamemode');
var manager = require('rooms.manager');
function isKeepable(s){
    return (s.structureType == STRUCTURE_EXTENSION ||
            s.structureType == STRUCTURE_SPAWN) &&
            s.energy < s.energyCapacity;

};

var print = utils.print;
var ERR = utils.ERR;
var VIP = utils.VIP;

var spawnKeeper = {

    name: "SpawnKeeper Controller",

    roleName: "spawnkeeper",

    getPriorities: function(){

        return _.map(manager.getControlledRooms(),(x)=>this.getPriority(x));
    },

    prepare: function(roomName) {
        return;
    },

    getPriority: function(roomName) {
        if(gameMode.isBootstrap(roomName)){
            return 0;
        }
        var room = Game.rooms[roomName];
        if(room == null){
            print(VIP, "Unknown room as priority arg in " + this.name);
            return 0;
        }
        var creeps = manager.getCreepsInRoom(roomName, this.roleName);
        var lvl = utils.getControllerLevel(room.controller);
        var desiredCreeps = 1;
        if(lvl >= 3) desiredCreeps = 2;
        if(lvl >= 5) desiredCreeps = 3;
        var priority = 0;
        if(creeps.length == 0){
            priority = 1.8;
        }else if(creeps.length < desiredCreeps){
            priority = 0.95;
        }
        return priority;
    },

    getEnergyThreshold: function(){
        return 300;
    },

    MAX_LEVEL: 5,
    getIdealCreep: function(energy, roomName){
        if(utils.getRoomLevel(roomName) >= 3){
            var basicSet = [CARRY, CARRY, MOVE];
            var baseName = "SK";
        }else{
            var basicSet = [CARRY, MOVE];
            var baseName = "LSK"; // Light SpawnKeeper
        }
        var basicCost = utils.calculateSpawnCost(basicSet);
        if(basicCost == 0){
            utils.print(2, "Something went really wrong with cost caluclation!");
        }
        var level = Math.floor(energy/basicCost);
        if(level == 0) return null;
        level = Math.min(level, this.MAX_LEVEL); // Do not create unnecessarily large creeps.

        var parts = [];
        for(var i=0; i<level;i++){
            parts = parts.concat(basicSet);
        }

        var creep = {
            parts: parts,
            name: "SK"+level,
            memory: {
                role: this.roleName,
                loading: false,
                target: null
            }
        };
        return creep;
    },

    getCreeps: function() {
        return _.filter(Game.creeps, (creep) => creep.memory.role == this.roleName);
    },

    creepsCount: function() {
        return this.getCreeps().length;
    },

    run: function(){
        var creeps = this.getCreeps();
        for(var i=0; i<creeps.length; i++){
            this.move(creeps[i]);
        }
    },

    findNewTarget: function(room, pos){
        var spawn = room.find(FIND_STRUCTURES, {filter:(s)=>s.structureType==STRUCTURE_SPAWN})[0];
        if(spawn == null){
            print(ERR, "Can't find new spawnkeeper target in room " + room.name);
            return null;
        }
        var res = spawn.room.find(FIND_STRUCTURES,{filter: isKeepable});
        var min = _.min(res, (x) => x.pos.getRangeTo(pos));
        return min;
    },

    /** @param {Creep} creep **/
    move: function(creep) {
        if(creep.memory.loading && creep.carry.energy == 0) {
            creep.memory.loading = false;
            creep.say('harvesting');
        }
        if(!creep.memory.loading && creep.carry.energy >= creep.carryCapacity) {
            creep.memory.loading = true;
            creep.say('loading');
        }

        if(creep.memory.loading) {
            if(manager.getRoomStatus(creep.room.name)!=manager.OWN){
                var roomName = manager.getClosestRoom(creep.room.name, manager.OWN);
                creep.moveTo(new RoomPosition(25,25,roomName));
            }
            if(creep.memory.target == null){
                var t = this.findNewTarget(creep.room, creep.pos);
                if(t != null) creep.memory.target = t.id;
                else          creep.memory.target = null;
            }
            var target = Game.getObjectById(creep.memory.target);
            if(target == null){
                creep.memory.target = null;
                return;
                //Don't have anything useful to do. Let's do something;
                var cnt = containers.getContainerStorages();
                creep.say("AAA");
                if(cnt.length>0){
                    utils.print(4, "Maybe " + cnt[0].id);
                    target = cnt[0].id;
                    creep.memory.target = target;
                }
                return;
            }
            if(target.structureType != STRUCTURE_STORAGE && target.energy == target.energyCapacity){
                // Target full
                creep.memory.target = null;
                return this.move(creep); // Potentially dangerous loop
            }
            var res = creep.transfer(target,RESOURCE_ENERGY);
            if(res == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }
        }
        else {
            containers.creepGetEnergy(creep);
        }
    }
};

module.exports = spawnKeeper;
